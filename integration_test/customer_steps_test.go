package integration_test

import (
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"foundation/internal/server/rest/customers"
	"github.com/cucumber/godog"
	"strconv"
)

const (
	lastCustomerId = "LAST_CUSTOMER_ID"
)

func (a *apiFeature) theCustomerExists(ctx context.Context, email string) error {
	query := "SELECT id, forename, surname, dob from customers WHERE email = $1"
	row := postgresDB.DB.QueryRowContext(ctx, query, email)
	var id int64
	var forename string
	var surname string
	var dob sql.NullTime
	err := row.Scan(&id, &forename, &surname, &dob)
	if err != nil {
		return err
	}
	return nil
}

func (a *apiFeature) customersExist(ctx context.Context, table *godog.Table) (context.Context, error) {
	var id int64
	query := "INSERT INTO customers (forename, surname, email, phone) values ($1, $2, $3, $4) returning id"
	for i := range table.Rows {
		forename := table.Rows[i].Cells[0].Value
		surname := table.Rows[i].Cells[1].Value
		email := table.Rows[i].Cells[2].Value
		phone := table.Rows[i].Cells[3].Value
		err := postgresDB.DB.QueryRowContext(ctx, query, forename, surname, email, phone).Scan(&id)
		if err != nil {
			return ctx, err
		}
	}
	ctx = setContextVariable(ctx, lastCustomerId, strconv.FormatInt(id, 10))
	return ctx, nil
}

func (a *apiFeature) customersResponseContains(ctx context.Context, table *godog.Table) error {
	actualResp, ok := ctx.Value(godogsResponseCtxKey{}).(response)
	if !ok {
		return errors.New("there are no godogs available")
	}

	var data customers.CustomersResponse
	err := json.Unmarshal(actualResp.body, &data)
	if err != nil {
		return err
	}
	if len(data.Customers) != len(table.Rows) {
		return fmt.Errorf("mismatch of number of customers. Got %d Expected %d", len(data.Customers), len(table.Rows))
	}
	for i, _ := range data.Customers {
		if *data.Customers[i].Email != table.Rows[i].Cells[0].Value {
			return fmt.Errorf("mismatch email in row %d. Got %s Expected %s", i, *data.Customers[i].Email, table.Rows[i].Cells[0].Value)
		}
	}
	return nil
}

func (a *apiFeature) customerResponseContains(ctx context.Context, email string) error {
	actualResp, ok := ctx.Value(godogsResponseCtxKey{}).(response)
	if !ok {
		return errors.New("there are no godogs available")
	}

	var data customers.CustomerResponse
	err := json.Unmarshal(actualResp.body, &data)
	if err != nil {
		return err
	}
	if *data.Email != email {
		return fmt.Errorf("mismatch email. Got %s Expected %s", *data.Email, email)
	}
	return nil
}
