package integration_test

import (
	"bytes"
	"context"
	"text/template"
)

type variablesCtxKey struct{}

func getContextVariables(ctx context.Context) map[string]string {
	variables, ok := ctx.Value(variablesCtxKey{}).(map[string]string)
	if variables == nil || !ok {
		variables = make(map[string]string)
	}
	return variables
}

func setContextVariable(ctx context.Context, key, value string) context.Context {
	variables := getContextVariables(ctx)
	variables[key] = value
	return context.WithValue(ctx, variablesCtxKey{}, variables)
}

func setVariablesInContent(ctx context.Context, content string) string {
	variableMap := getContextVariables(ctx)
	t := template.New("test")
	tt, err := t.Parse(content)
	if err != nil {
		panic(err)
	}
	buf := new(bytes.Buffer)
	if err = tt.Execute(buf, &variableMap); err != nil {
		panic(err)
	}
	return buf.String()
}
