package integration_test

import (
	"context"
	"database/sql"
	"foundation/internal/db"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/modules/postgres"
	"github.com/testcontainers/testcontainers-go/wait"
	"log"
	"os"
	"time"

	_ "github.com/lib/pq"
)

const (
	dbUsername = "postgres"
	dbPassword = "password123"
	dbName     = "test"
)

func SetupDBContainer() (func(), *db.Postgres, *string, error) {
	logger := log.New(os.Stdout, "", 0)
	logger.Println("Setting up Postgres Container")
	ctx := context.Background()

	pgContainer, err := postgres.RunContainer(ctx,
		testcontainers.WithImage("postgres:15.3-alpine"),
		postgres.WithDatabase(dbName),
		postgres.WithUsername(dbUsername),
		postgres.WithPassword(dbPassword),
		testcontainers.WithWaitStrategy(
			wait.ForLog("database system is ready to accept connections").
				WithOccurrence(2).WithStartupTimeout(5*time.Second)),
	)
	if err != nil {
		return nil, nil, nil, err
	}

	connStr, err := pgContainer.ConnectionString(ctx, "sslmode=disable")
	if err != nil {
		return nil, nil, nil, err
	}
	database, err := sql.Open("postgres", connStr)
	if err != nil {
		return nil, nil, nil, err
	}
	err = db.RunMigrations(connStr)
	if err != nil {
		return nil, nil, nil, err
	}

	closeContainer := func() {
		if err := pgContainer.Terminate(ctx); err != nil {
			logger.Printf("Error terminating postgres: %v\n", err)
		}
	}

	return closeContainer, &db.Postgres{DB: database}, &connStr, nil
}
