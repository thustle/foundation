Feature: Resend a user activation token

  Scenario: User successfully attempts to retrieve a new activation code
    Given the user "Brian" exists with email "brian@company.com" and password "password123" and activated "false"
    And a "POST" request is sent to "/v1/users/activation" with payload:
      """
      {
        "email":"brian@company.com"
      }
      """
    And an email to "brian@company.com" was sent with the subject "Activate your Foundation account"
    And a "PUT" request is sent to "/v1/users/activated" with payload:
      """
      {
        "token":"{{.VAR_TOKEN}}"
      }
      """
    Then the response code should be 200
    And the user with email "brian@company.com" should be activated

  Scenario: User unsuccessfully attempts to resend an activation code to an unknown email
    Given the user "Brian" exists with email "brian@company.com" and password "password123" and activated "false"
    And a "POST" request is sent to "/v1/users/activation" with payload:
      """
      {
        "email":"unknown@company.com"
      }
      """
    Then the response code should be 400
    And the response should contain errors:
      |email|no matching email address found|

  Scenario: User unsuccessfully attempts to activate an already activated user
    Given the user "Brian" exists with email "brian@company.com" and password "password123" and activated "true"
    When a "POST" request is sent to "/v1/users/activation" with payload:
      """
      {
        "email":"brian@company.com"
      }
      """
    Then the response code should be 400
    And the response should contain errors:
      |email|user has already been activated|
