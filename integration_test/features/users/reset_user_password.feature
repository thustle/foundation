Feature: Resend a user activation token

  Background:
    Given the user "Brian" exists with email "brian@company.com" and password "password123" and activated "true"

  Scenario: User resets their password and is able to log in with the new password
    Given a "POST" request is sent to "/v1/users/password-reset" with payload:
      """
      {
        "email":"brian@company.com"
      }
      """
    And an email to "brian@company.com" was sent with the subject "Reset your Foundation password"
    And a "PUT" request is sent to "/v1/users/password" with payload:
      """
      {
        "token":"{{.VAR_TOKEN}}",
        "password":"newpassword"
      }
      """
    And the response code should be 200
    When a "POST" request is sent to "/v1/users/authentication" with payload:
      """
      {
        "email":"brian@company.com",
        "password":"newpassword"
      }
      """
    Then the response code should be 200
