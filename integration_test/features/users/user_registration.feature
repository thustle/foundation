Feature: User Registration

  Scenario: User successfully attempts to register on the system
    When a "POST" request is sent to "/v1/users" with payload:
      """
      {
        "name":"Brian",
        "email":"brian@company.com",
        "password":"password123"
      }
      """
    Then the response code should be 201
    And the response ID should exist in the "users" table
    And an email to "brian@company.com" was sent with the subject "Welcome to Foundation!"

  Scenario: User unsuccessfully attempts to register on the system with invalid data
    When a "POST" request is sent to "/v1/users" with payload:
      """
      {
        "name":"",
        "email":"briancompany.com",
        "password":"pw"
      }
      """
    Then the response code should be 400
    And the response should contain errors:
    |email|must be a valid email address|
    |name|must be provided|
    |password|must be at least 8 bytes long|

  Scenario: User unsuccessfully attempts to register on the system with invalid JSON
    When a "POST" request is sent to "/v1/users" with payload:
      """
        name=Brian&email=brian@company.com&password=password
      """
    Then the response code should be 400
    And the response should contain errors:
      |-|body contains badly formed JSON (at character 4)|
