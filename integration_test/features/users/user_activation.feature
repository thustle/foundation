Feature: User Activation

  Scenario: User successfully attempts to activate their registration
    Given a "POST" request is sent to "/v1/users" with payload:
      """
      {
        "name":"Brian",
        "email":"brian@company.com",
        "password":"password123"
      }
      """
    And an email to "brian@company.com" was sent with the subject "Welcome to Foundation!"
    When a "PUT" request is sent to "/v1/users/activated" with payload:
      """
      {
        "token":"{{.VAR_TOKEN}}"
      }
      """
    Then the response code should be 200
    And the user with email "brian@company.com" should be activated

  Scenario: User unsuccessfully attempts to activate their registration with bad token
    When a "PUT" request is sent to "/v1/users/activated" with payload:
      """
      {
        "token":"INVALID_TOKEN"
      }
      """
    Then the response code should be 400
    And the response should contain errors:
      |token|must be 26 bytes long|

  Scenario: User unsuccessfully attempts to activate their registration with unknown token
    When a "PUT" request is sent to "/v1/users/activated" with payload:
      """
      {
        "token":"ABC123ABC123ABC123ABC123AB"
      }
      """
    Then the response code should be 400
    And the response should contain errors:
      |token|invalid or expired activation token|
