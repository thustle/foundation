Feature: Search for customers

  Background:
    Given the user "Brian" exists with email "brian@company.com" and password "password123" and activated "true"
    And the user is logged in with email "brian@company.com" and password "password123"
    And the following customers exist in the system:
      |Claire|Lupin|clupin@company.com|07788901234|
      |Clare|Alexander|calexander@company.com|07799901234|

  Scenario: User unsuccessfully attempts to search for customers without required permissions
    Given a "GET" request is made to "/v1/customers"
    Then the response code should be 403

  Scenario: User attempts to retrieve customers
    Given the user with email "brian@company.com" has permission "customers:read"
    And a "GET" request is made to "/v1/customers"
    Then the response code should be 200
    And the result contains the following customers:
      |clupin@company.com|
      |calexander@company.com|

  Scenario: User attempts to retrieve customers ordering by
    Given the user with email "brian@company.com" has permission "customers:read"
    And a "GET" request is made to "/v1/customers?sort=surname"
    Then the response code should be 200
    And the result contains the following customers:
      |calexander@company.com|
      |clupin@company.com|

  Scenario: User attempts to retrieve customers filtering by surname
    Given the user with email "brian@company.com" has permission "customers:read"
    And a "GET" request is made to "/v1/customers?surname=lupin"
    Then the response code should be 200
    And the result contains the following customers:
      |clupin@company.com|
