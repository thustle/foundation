Feature: Get customer

  Background:
    Given the user "Brian" exists with email "brian@company.com" and password "password123" and activated "true"
    And the user is logged in with email "brian@company.com" and password "password123"
    And the following customers exist in the system:
      |Claire|Lupin|clupin@company.com|07788901234|
      |Clare|Alexander|calexander@company.com|07799901234|

  Scenario: User unsuccessfully attempts to search for customer without required permissions
    Given a "GET" request is made to "/v1/customers/49494949"
    Then the response code should be 403

  Scenario: User unsuccessfully attempts to search for customer with incorrect ID
    Given the user with email "brian@company.com" has permission "customers:read"
    And a "GET" request is made to "/v1/customers/49494949"
    Then the response code should be 404

  Scenario: User attempts to retrieve customer by ID
    Given the user with email "brian@company.com" has permission "customers:read"
    And a "GET" request is made to "/v1/customers/{{.LAST_CUSTOMER_ID}}"
    Then the response code should be 200
    And the result contains customer "calexander@company.com"
