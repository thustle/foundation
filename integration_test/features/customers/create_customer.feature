Feature: Get a customer authenticated page

  Background:
    Given the user "Brian" exists with email "brian@company.com" and password "password123" and activated "true"
    And the user is logged in with email "brian@company.com" and password "password123"

  Scenario: User unsuccessfully attempts to create a customer without required permissions
    Given a "POST" request is sent to "/v1/customers" with payload:
    """
    {
      "forename":"Claire",
      "surname":"Lupin"
    }
    """
    Then the response code should be 403

  Scenario: User successfully attempts to create a customer
    Given the user with email "brian@company.com" has permission "customers:write"
    And a "POST" request is sent to "/v1/customers" with payload:
    """
    {
      "forename":"Claire",
      "surname":"Lupin",
      "dob":"1969-04-23",
      "email":"clupin@company.com",
      "phone":"07788901234"
    }
    """
    Then the response code should be 201
    And the customer with email "clupin@company.com" exists
