Feature: Healthcheck

  Scenario: User successfully retrieves the app's health data
    When a "GET" request is made to "/v1/health"
    Then the response code should be 200
