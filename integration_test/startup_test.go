package integration_test

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"foundation/config"
	"foundation/internal/db"
	"foundation/internal/domain"
	"foundation/internal/logger"
	"foundation/internal/server"
	"github.com/cucumber/godog"
	"io"
	"net/http"
	"os"
	"strconv"
	"sync"
	"testing"
)

var postgresDB *db.Postgres
var configuration *config.Config
var emailWebPort string

func TestMain(m *testing.M) {
	flag.Parse()
	if testing.Short() {
		return
	}
	configuration = config.NewConfig()
	log := logger.New(configuration, os.Stdout)
	var err error
	var smtpPort string
	var terminateEmailContainer func()

	terminateEmailContainer, smtpPort, emailWebPort, err = SetupEmailContainer()
	if err != nil {
		log.Error("Error creating email container", "err", err)
	}
	defer func() {
		if terminateEmailContainer != nil {
			terminateEmailContainer()
		}
	}()

	var terminateDBContainer func()
	var connStr *string

	terminateDBContainer, postgresDB, connStr, err = SetupDBContainer()
	defer func() {
		if terminateDBContainer != nil {
			terminateDBContainer()
		}
	}()
	if err != nil {
		log.Error("Error creating DB container", "err", err)
	}
	setUpTestConfig(configuration, *connStr, smtpPort)

	waitGroup := sync.WaitGroup{}
	services := domain.BuildServices(postgresDB, configuration, log, &waitGroup)
	go func() {
		err = server.StartServers(configuration, log, services, &waitGroup)
		if err != nil {
			log.Error("Error starting server", "err", err)
		}
	}()

	m.Run()
}

func setUpTestConfig(c *config.Config, connStr string, smtpPort string) {
	c.URL = connStr
	c.SMTPEnabled = true
	c.SMTPPort, _ = strconv.Atoi(smtpPort)
	c.SMTPHost = "localhost"
}

func TestFeatures(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping feature tests")
	}
	suite := godog.TestSuite{
		ScenarioInitializer: InitialiseScenario,
		Options: &godog.Options{
			Format: "pretty",
			Paths:  []string{"features"},

			TestingT: t,
		},
	}
	if suite.Run() != 0 {
		t.Fatal("non-zero status returned, failed to run feature tests")
	}
}

type godogsResponseCtxKey struct{}
type apiFeature struct{}
type response struct {
	status int
	body   []byte
}

func (a *apiFeature) sendRequest(ctx context.Context, method, route string) (context.Context, error) {
	route = setVariablesInContent(ctx, route)
	return a.sendRequestWithContent(ctx, method, route, nil)
}

func (a *apiFeature) sendRequestWithContent(ctx context.Context, method, route string, payloadDoc *godog.DocString) (context.Context, error) {
	var reqBody []byte
	if payloadDoc != nil {
		reqBody = []byte(setVariablesInContent(ctx, payloadDoc.Content))
	}

	path := fmt.Sprintf("http://localhost:%s%s", configuration.Port, route)
	req, _ := http.NewRequest(method, path, bytes.NewReader(reqBody))
	req.Header.Set("Content-Type", "application/json")
	userToken, ok := ctx.Value(userTokenCtxKey{}).(string)
	if ok {
		req.Header.Set("Authorization", "Bearer "+userToken)
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return ctx, err
	}
	defer resp.Body.Close()

	respBody, _ := io.ReadAll(resp.Body)
	actual := response{
		status: resp.StatusCode,
		body:   respBody,
	}
	return context.WithValue(ctx, godogsResponseCtxKey{}, actual), nil
}

func (a *apiFeature) theResponseCodeShouldBe(ctx context.Context, expectedStatus int) error {
	resp, ok := ctx.Value(godogsResponseCtxKey{}).(response)
	if !ok {
		return errors.New("no response in context")
	}
	if expectedStatus != resp.status {
		if resp.status >= 400 {
			return fmt.Errorf("expected response code to be: %d, but was: %d. Message: %s", expectedStatus, resp.status, resp.body)
		}
		return fmt.Errorf("expected response code to be: %d, but was: %d", expectedStatus, resp.status)
	}
	return nil
}

func (a *apiFeature) theResponseIDShouldExistInTable(ctx context.Context, tableName string) error {
	actualResp, ok := ctx.Value(godogsResponseCtxKey{}).(response)
	if !ok {
		return errors.New("there are no godogs available")
	}

	var data map[string]any
	err := json.Unmarshal(actualResp.body, &data)
	if err != nil {
		return err
	}

	query := fmt.Sprintf("SELECT ID from %s WHERE ID = %d", tableName, int(data["id"].(float64)))
	result, err := postgresDB.DB.Exec(query)
	if err != nil {
		return err
	}
	if rows, _ := result.RowsAffected(); rows != 1 {
		return fmt.Errorf("error checking for ID %v in %s", data["id"], tableName)
	}
	return nil
}

func InitialiseScenario(ctx *godog.ScenarioContext) {
	api := apiFeature{}

	ctx.Before(func(ctx context.Context, sc *godog.Scenario) (context.Context, error) {
		deleteEmails()
		_, err := postgresDB.DB.Exec("DELETE from customers")
		if err != nil {
			return nil, err
		}
		_, err = postgresDB.DB.Exec("DELETE from tokens")
		if err != nil {
			return nil, err
		}
		_, err = postgresDB.DB.Exec("DELETE from users_permissions")
		if err != nil {
			return nil, err
		}
		_, err = postgresDB.DB.Exec("DELETE from users")
		if err != nil {
			return nil, err
		}
		return ctx, nil
	})

	ctx.Step(`^a "([^"]*)" request is sent to "([^"]*)" with payload:$`, api.sendRequestWithContent)
	ctx.Step(`^a "([^"]*)" request is made to "([^"]*)"$`, api.sendRequest)
	ctx.Step(`^the response code should be (\d+)$`, api.theResponseCodeShouldBe)
	ctx.Step(`^the response ID should exist in the "([^"]*)" table$`, api.theResponseIDShouldExistInTable)
	ctx.Step(`^an email to "([^"]*)" was sent with the subject "([^"]*)"$`, api.theEmailWasSent)
	ctx.Step(`^the response should contain errors:$`, api.theResponseShouldContainErrors)
	ctx.Step(`^the user with email "([^"]*)" should be activated$`, api.theUserShouldBeActivated)
	ctx.Step(`^the user "([^"]*)" exists with email "([^"]*)" and password "([^"]*)" and activated "([^"]*)"$`, api.theUserExists)
	ctx.Step(`^the user is logged in with email "([^"]*)" and password "([^"]*)"$`, api.theUserLogsIn)
	ctx.Step(`^the user with email "([^"]*)" has permission "([^"]*)"$`, api.theUserHasPermission)
	ctx.Step(`^the customer with email "([^"]*)" exists$`, api.theCustomerExists)
	ctx.Step(`^the following customers exist in the system:$`, api.customersExist)
	ctx.Step(`^the result contains the following customers:$`, api.customersResponseContains)
	ctx.Step(`^the result contains customer "([^"]*)"$`, api.customerResponseContains)
}

func deleteEmails() {
	req, _ := http.NewRequest(http.MethodDelete, "http://localhost:"+emailWebPort+"/api/v1/messages", nil)
	req.Header.Set("Content-Type", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Printf("Error deleting emails, %v\n", err)
	}
	defer resp.Body.Close()
}
