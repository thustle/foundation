package integration_test

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"regexp"
	"slices"
	"time"
)

const (
	varToken = "VAR_TOKEN"
)

var tokenRegex, _ = regexp.Compile(`"([A-Z0-9]{26})"`)

type emailRespHeaders struct {
	Subject []string
	To      []string
}
type emailRespContent struct {
	Headers emailRespHeaders
	Body    string
}
type emailResp struct {
	ID      string `json:"ID"`
	Content emailRespContent
}

func (a *apiFeature) theEmailWasSent(ctx context.Context, recipient, title string) (context.Context, error) {
	var err error
	var resp *http.Response
	var data []emailResp
	for i := 1; i <= 3; i++ {
		fmt.Println("Checking for email...")
		req, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("http://localhost:%s/api/v1/messages", emailWebPort), nil)
		req.Header.Set("Content-Type", "application/json")
		resp, err = http.DefaultClient.Do(req)
		if err != nil {
			return ctx, err
		}
		defer resp.Body.Close()

		respBody, err := io.ReadAll(resp.Body)
		if err != nil {
			return ctx, err
		}

		err = json.Unmarshal(respBody, &data)
		if err != nil {
			return ctx, err
		}

		if len(data) > 0 {
			break
		}

		time.Sleep(700 * time.Millisecond)
	}

	if len(data) != 1 {
		return ctx, fmt.Errorf("unexpected number of emails sent: %d", len(data))
	}
	if !slices.Contains(data[0].Content.Headers.Subject, title) {
		return ctx, fmt.Errorf("email didn't contain title %s", title)
	}
	if !slices.Contains(data[0].Content.Headers.To, recipient) {
		return ctx, fmt.Errorf("email wasn't to receipient %s", recipient)
	}

	tokenInEmailMatch := tokenRegex.FindStringSubmatch(data[0].Content.Body)
	if len(tokenInEmailMatch) > 1 {
		ctx = setContextVariable(ctx, varToken, tokenInEmailMatch[1])
	}

	fmt.Println("Deleting read email...")
	req, _ := http.NewRequest(http.MethodDelete, fmt.Sprintf("http://localhost:%s/api/v1/messages/%s", emailWebPort, data[0].ID), nil)
	req.Header.Set("Content-Type", "application/json")
	resp, err = http.DefaultClient.Do(req)
	if err != nil {
		return ctx, err
	}
	defer resp.Body.Close()

	return ctx, nil
}
