package integration_test

import (
	"context"
	"fmt"
	_ "github.com/lib/pq"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
	"log"
	"os"
)

func SetupEmailContainer() (func(), string, string, error) {
	logger := log.New(os.Stdout, "", 0)
	logger.Println("Setting up Postgres Container")
	ctx := context.Background()
	req := testcontainers.ContainerRequest{
		Image:        "mailhog/mailhog",
		ExposedPorts: []string{"1025/tcp", "8025/tcp"},
		WaitingFor:   wait.ForHTTP("/").WithPort("8025/tcp"),
	}
	emailContainer, err := testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
		ContainerRequest: req,
		Started:          true,
	})
	if err != nil {
		return nil, "", "", err
	}
	closeContainer := func() {
		if err := emailContainer.Terminate(ctx); err != nil {
			log.Fatalf("Could not stop mailhog: %s", err)
		}
	}
	portMap, _ := emailContainer.Ports(ctx)
	for i, k := range portMap {
		fmt.Printf("%v %v\n", i, k[0].HostPort)
	}

	return closeContainer, portMap["1025/tcp"][0].HostPort, portMap["8025/tcp"][0].HostPort, nil
}
