package integration_test

import (
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"io"
	"net/http"
	"testing"
)

func TestHealthcheck(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test")
	}
	fmt.Println("Starting example")
	req, _ := http.NewRequest(http.MethodGet, "http://localhost:8080/v1/health", nil)
	req.Header.Set("Content-Type", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Errorf("Error calling healthcheck: %v", err)
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	assert.NoError(t, err)

	result := make(map[string]interface{})
	err = json.Unmarshal(body, &result)
	assert.NoError(t, err)

	assert.Equal(t, configuration.Version, result["version"])
	assert.Equal(t, configuration.Environment, result["environment"])
	assert.Equal(t, "available", result["status"])
}
