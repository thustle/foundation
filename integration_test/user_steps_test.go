package integration_test

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"foundation/internal/server/rest/users"
	"golang.org/x/crypto/bcrypt"
	"io"
	"net/http"
)

func (a *apiFeature) theUserShouldBeActivated(_ context.Context, email string) error {
	query := "SELECT activated from users WHERE email = $1"
	row := postgresDB.DB.QueryRow(query, email)
	isActivated := false
	err := row.Scan(&isActivated)
	if err != nil {
		return err
	}
	if !isActivated {
		return errors.New("user was not activated")
	}
	return nil
}

func (a *apiFeature) theUserExists(ctx context.Context, name, email, password, activated string) (context.Context, error) {
	query := "INSERT INTO users (name, email, password_hash, activated) values ($1, $2, $3, $4) returning id"
	hash, _ := bcrypt.GenerateFromPassword([]byte(password), 12)
	var id int64
	err := postgresDB.DB.QueryRowContext(ctx, query, name, email, hash, activated == "true").Scan(&id)
	if err != nil {
		return ctx, err
	}
	return ctx, nil
}

func (a *apiFeature) theUserHasPermission(ctx context.Context, email, permission string) error {
	query := `INSERT INTO users_permissions (user_id, permission_id) VALUES (
		(select id from users where email = $1),
		(select id from permissions where code = $2)
		)`
	_, err := postgresDB.DB.ExecContext(ctx, query, email, permission)
	if err != nil {
		return err
	}
	return nil
}

type userTokenCtxKey struct{}

func (a *apiFeature) theUserLogsIn(ctx context.Context, email, password string) (context.Context, error) {
	authRequest := users.AuthRequest{
		Email:    email,
		Password: password,
	}
	reqBody, err := json.Marshal(authRequest)
	if err != nil {
		return nil, err
	}

	path := fmt.Sprintf("http://localhost:%s/v1/users/authentication", configuration.Port)
	req, _ := http.NewRequest(http.MethodPost, path, bytes.NewReader(reqBody))
	req.Header.Set("Content-Type", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	respBody, _ := io.ReadAll(resp.Body)
	var response users.AuthenticationResponse
	err = json.Unmarshal(respBody, &response)
	if err != nil {
		return nil, err
	}
	return context.WithValue(ctx, userTokenCtxKey{}, response.Token.Plaintext), nil
}
