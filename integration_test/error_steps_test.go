package integration_test

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/cucumber/godog"
)

type respError struct {
	field, message string
}

func (a *apiFeature) theResponseShouldContainErrors(ctx context.Context, table *godog.Table) error {
	actualResp, ok := ctx.Value(godogsResponseCtxKey{}).(response)
	if !ok {
		return errors.New("there are no godogs available")
	}

	fmt.Println(string(actualResp.body))
	var data map[string]map[string]string
	err := json.Unmarshal(actualResp.body, &data)
	if err != nil {
		return err
	}

	respErrors := data["errors"]
	respErrs := make([]respError, len(respErrors))
	for field, message := range respErrors {
		respErrs = append(respErrs, respError{
			field:   field,
			message: message,
		})
	}
	expErrs := make([]respError, len(table.Rows))
	for i := range table.Rows {
		field := table.Rows[i].Cells[0]
		message := table.Rows[i].Cells[1]
		expErrs = append(expErrs, respError{
			field:   field.Value,
			message: message.Value,
		})
	}
	if !errorsMatch(expErrs, respErrs) {
		return fmt.Errorf("errors don't match. Got: %v", respErrors)
	}
	return nil
}

func errorsMatch(listA, listB []respError) bool {
	aLen := len(listA)
	bLen := len(listB)
	if aLen != bLen {
		return false
	}

	visited := make([]bool, bLen)
	for i := 0; i < aLen; i++ {
		found := false
		element := listA[i]
		for j := 0; j < bLen; j++ {
			if visited[j] {
				continue
			}
			if element.field == listB[j].field && element.message == listB[j].message {
				visited[j] = true
				found = true
				break
			}
		}
		if !found {
			return false
		}
	}
	return true
}
