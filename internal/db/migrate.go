package db

import (
	"errors"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/source/iofs"
	"log"
	"os"

	_ "github.com/golang-migrate/migrate/v4/database/postgres"
)

func RunMigrations(connectionString string) error {
	logger := log.New(os.Stdout, "", log.Ldate|log.Ltime)
	source, err := iofs.New(MigrationFiles, "migrations")
	if err != nil {
		logger.Printf("Error finding migrations files: %v", err)
		return err
	}
	m, err := migrate.NewWithSourceInstance("iofs", source, connectionString)
	if err != nil {
		logger.Printf("Error creating connection for migrations: %v", err)
		return err
	}
	defer m.Close()

	err = m.Up()
	if err != nil && !errors.Is(err, migrate.ErrNoChange) {
		logger.Printf("Error applying migrations: %v", err)
		return err
	}
	version, b, _ := m.Version()
	logger.Printf("Migrated DB to version %06d, dirty = %t", version, b)
	return nil
}
