// Package postgres implements postgres connection.
package db

import (
	"context"
	"database/sql"
	"foundation/config"
	"sync"
	"time"

	_ "github.com/lib/pq"
)

// Postgres -.
type Postgres struct {
	maxPoolSize int
	DB          *sql.DB
}

var pg *Postgres
var hdlOnce sync.Once

// NewOrGetSingleton -.
func NewOrGetSingleton(config *config.Config) *Postgres {
	hdlOnce.Do(func() {
		postgres, err := initPg(config)
		if err != nil {
			panic(err)
		}
		pg = postgres
	})
	return pg
}

func initPg(config *config.Config) (*Postgres, error) {
	pg = &Postgres{
		maxPoolSize: config.PG.PoolMax,
	}
	db, err := sql.Open("postgres", config.PG.URL)
	if err != nil {
		return nil, err
	}
	db.SetMaxOpenConns(config.PG.PoolMax)
	db.SetMaxIdleConns(config.PG.MaxIdle)
	duration, err := time.ParseDuration(config.PG.MaxIdleTime)
	if err != nil {
		return nil, err
	}
	db.SetConnMaxIdleTime(duration)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	err = db.PingContext(ctx)
	if err != nil {
		return nil, err
	}
	pg.DB = db

	return pg, nil
}

// Close -.
func (p *Postgres) Close() {
	if p.DB != nil {
		p.DB.Close()
	}
}
