CREATE TABLE IF NOT EXISTS customers (
    id bigserial PRIMARY KEY,
    created_at timestamp(0) with time zone NOT NULL DEFAULT NOW(),
    created_by bigint NULL,
    forename text NOT NULL,
    surname text NOT NULL,
    dob date NULL,
    email citext UNIQUE NULL,
    phone text NULL,
    version integer NOT NULL DEFAULT 1
);
