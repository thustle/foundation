package metrics

import (
	"expvar"
	"net/http"
	"sync"
)

type V1Api struct{}

var metricsOnce sync.Once
var metricsV1Api *V1Api

func NewMetricsV1API() *V1Api {
	metricsOnce.Do(func() {
		metricsV1Api = &V1Api{}
	})
	return metricsV1Api
}

func (api *V1Api) ConfigureRoutes(mux *http.ServeMux, handler func(http.Handler) http.Handler) {
	mux.Handle("GET /v1/metrics", handler(expvar.Handler()))
}
