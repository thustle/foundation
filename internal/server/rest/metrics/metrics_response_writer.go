package metrics

import "net/http"

type ResponseWriter struct {
	StatusCode    int
	headerWritten bool
	http.ResponseWriter
}

func NewMetricsResponseWriter(w http.ResponseWriter) *ResponseWriter {
	return &ResponseWriter{
		StatusCode:     http.StatusOK,
		ResponseWriter: w,
	}
}

func (mw *ResponseWriter) WriteHeader(statusCode int) {
	mw.ResponseWriter.WriteHeader(statusCode)
	if !mw.headerWritten {
		mw.StatusCode = statusCode
		mw.headerWritten = true
	}
}

func (mw *ResponseWriter) Write(b []byte) (int, error) {
	mw.headerWritten = true
	return mw.ResponseWriter.Write(b)
}

func (mw *ResponseWriter) Unwrap() http.ResponseWriter {
	return mw.ResponseWriter
}
