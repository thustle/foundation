package middleware

import (
	"context"
	"fmt"
	error2 "foundation/internal/domain/error"
	"foundation/internal/domain/user"
	"foundation/internal/server/rest/util"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"io"
	"log/slog"
	"net/http"
	"net/http/httptest"
	"strconv"
	"sync"
	"testing"
)

func TestAuthenticationAnonymous(t *testing.T) {
	rr := httptest.NewRecorder()
	r, err := http.NewRequest(http.MethodGet, "/", nil)
	if err != nil {
		t.Fatal(err)
	}
	mw := NewMiddleware(nil, nil, nil, nil)

	next := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		usr := mw.contextGetUser(r)
		_, err := w.Write([]byte(fmt.Sprintf("Anonymous=%s", strconv.FormatBool(usr.IsAnonymous()))))
		if err != nil {
			t.Error(err)
		}
	})

	mw.Authenticate(next).ServeHTTP(rr, r)
	rs := rr.Result()

	defer rs.Body.Close()
	body, err := io.ReadAll(rs.Body)
	if err != nil {
		t.Error(err)
	}
	assert.Equal(t, "Anonymous=true", string(body))
}

type mockUserService struct{ mock.Mock }

func (m *mockUserService) GetByEmail(_ context.Context, _ string) (*user.User, error) {
	return nil, nil
}
func (m *mockUserService) Create(_ context.Context, _ *user.User) error { return nil }
func (m *mockUserService) ActivateUser(_ context.Context, _ string) (*user.User, error) {
	return nil, nil
}
func (m *mockUserService) CreatePasswordResetToken(_ context.Context, _ string) (*user.Token, error) {
	return nil, nil
}
func (m *mockUserService) UpdatePassword(context.Context, string, string) error { return nil }
func (m *mockUserService) CreateActivationToken(context.Context, string) error  { return nil }
func (m *mockUserService) Authenticate(context.Context, string, string) (*user.Token, error) {
	return nil, nil
}
func (m *mockUserService) GetUserPermissions(ctx context.Context, userID int64) (user.Permissions, error) {
	args := m.Called(ctx, userID)
	return args.Get(0).(user.Permissions), args.Error(1)
}
func (m *mockUserService) GetByToken(ctx context.Context, scope string, token string) (*user.User, error) {
	args := m.Called(ctx, scope, token)
	return args.Get(0).(*user.User), args.Error(1)
}

func TestAuthenticationToken(t *testing.T) {
	log := slog.Default()
	wg := sync.WaitGroup{}
	helper := util.NewHelper(log, &wg)

	testCases := []struct {
		name, token  string
		usr          *user.User
		tokenErr     error
		expectedBody string
	}{
		{
			name:         "Valid token",
			token:        "Bearer 12345678901234567890123456",
			usr:          &user.User{Email: "test@test.com"},
			tokenErr:     nil,
			expectedBody: "User Email = test@test.com",
		},
		{
			name:         "Bad bearer token",
			token:        "Bearer BAD_TOKEN",
			usr:          nil,
			tokenErr:     nil,
			expectedBody: "{\"errors\":{\"-\":\"invalid or missing authentication token\"}}\n",
		},
		{
			name:         "Non-bearer token",
			token:        "BAD_TOKEN",
			usr:          nil,
			tokenErr:     nil,
			expectedBody: "{\"errors\":{\"-\":\"invalid or missing authentication token\"}}\n",
		},
		{
			name:         "Invalid token",
			token:        "Bearer 12345678901234567890123456",
			usr:          nil,
			tokenErr:     error2.ErrRecordNotFound,
			expectedBody: "{\"errors\":{\"-\":\"invalid or missing authentication token\"}}\n",
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			rr := httptest.NewRecorder()
			r, err := http.NewRequest(http.MethodGet, "/", nil)
			r.Header.Set("Authorization", tc.token)
			if err != nil {
				t.Fatal(err)
			}
			mockUsrService := mockUserService{}
			mockUsrService.On("GetByToken", mock.Anything, user.ScopeAuthentication, tc.token[7:]).Return(tc.usr, tc.tokenErr)
			mw := &Middleware{
				config:      nil,
				log:         log,
				helper:      helper,
				userService: &mockUsrService,
			}

			next := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				usr := mw.contextGetUser(r)
				_, err := w.Write([]byte(fmt.Sprintf("User Email = %s", usr.Email)))
				if err != nil {
					t.Error(err)
				}
			})

			mw.Authenticate(next).ServeHTTP(rr, r)
			rs := rr.Result()

			defer rs.Body.Close()
			body, err := io.ReadAll(rs.Body)
			if err != nil {
				t.Error(err)
			}
			assert.Equal(t, tc.expectedBody, string(body))
		})
	}
}

func TestAuthenticationRequired(t *testing.T) {
	log := slog.Default()
	wg := sync.WaitGroup{}
	helper := util.NewHelper(log, &wg)

	testCases := []struct {
		name, token  string
		usr          *user.User
		authRequired bool
		expectedBody string
	}{
		{
			name:  "Authenticated User",
			token: "12345678901234567890123456",
			usr: &user.User{
				Email:     "test@test.com",
				Activated: true,
			},
			authRequired: false,
			expectedBody: "User Anonymous = false",
		},
		{
			name:         "Anonymous User",
			token:        "",
			usr:          nil,
			authRequired: false,
			expectedBody: "User Anonymous = true",
		},
		{
			name:  "Authenticated User accesses auth-required path",
			token: "12345678901234567890123456",
			usr: &user.User{
				Email:     "test@test.com",
				Activated: true,
			},
			authRequired: true,
			expectedBody: "User Anonymous = false",
		},
		{
			name:  "Authenticated but not activated User accesses auth-required path",
			token: "12345678901234567890123456",
			usr: &user.User{
				Email:     "test@test.com",
				Activated: false,
			},
			authRequired: true,
			expectedBody: "{\"errors\":{\"-\":\"your user account must be activated to access this resource\"}}\n",
		},
		{
			name:         "Anonymous User accesses auth-required path",
			token:        "",
			usr:          nil,
			authRequired: true,
			expectedBody: "{\"errors\":{\"-\":\"you must be authenticated to access this resource\"}}\n",
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			rr := httptest.NewRecorder()
			r, err := http.NewRequest(http.MethodGet, "/", nil)
			if tc.token != "" {
				r.Header.Set("Authorization", "Bearer "+tc.token)
			}
			if err != nil {
				t.Fatal(err)
			}
			mockUsrService := mockUserService{}
			mockUsrService.On("GetByToken", mock.Anything, user.ScopeAuthentication, tc.token).Return(tc.usr, nil)
			mw := &Middleware{
				config:      nil,
				log:         log,
				helper:      helper,
				userService: &mockUsrService,
			}

			next := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				usr := mw.contextGetUser(r)
				_, err := w.Write([]byte(fmt.Sprintf("User Anonymous = %t", usr.IsAnonymous())))
				if err != nil {
					t.Error(err)
				}
			})

			if tc.authRequired {
				mw.Authenticate(mw.RequireActivatedUser(next)).ServeHTTP(rr, r)
			} else {
				mw.Authenticate(next).ServeHTTP(rr, r)
			}
			rs := rr.Result()

			defer rs.Body.Close()
			body, err := io.ReadAll(rs.Body)
			if err != nil {
				t.Error(err)
			}
			assert.Equal(t, tc.expectedBody, string(body))
		})
	}
}

func TestPermissionRequired(t *testing.T) {
	log := slog.Default()
	wg := sync.WaitGroup{}
	helper := util.NewHelper(log, &wg)

	testCases := []struct {
		name, token  string
		usr          *user.User
		usrPerms     user.Permissions
		expectedBody string
	}{
		{
			name:  "User with perms",
			token: "12345678901234567890123456",
			usr: &user.User{
				ID:        2,
				Email:     "test@test.com",
				Activated: true,
			},
			usrPerms:     user.Permissions{"read", "write"},
			expectedBody: "User allowed",
		},
		{
			name:  "User without perms",
			token: "12345678901234567890123456",
			usr: &user.User{
				ID:        2,
				Email:     "test@test.com",
				Activated: true,
			},
			usrPerms:     user.Permissions{"read"},
			expectedBody: "{\"errors\":{\"-\":\"your user account doesn't have the necessary permissions to access this resource\"}}\n",
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			rr := httptest.NewRecorder()
			r, err := http.NewRequest(http.MethodGet, "/", nil)
			r.Header.Set("Authorization", "Bearer "+tc.token)
			if err != nil {
				t.Fatal(err)
			}
			mockUsrService := mockUserService{}
			mockUsrService.On("GetByToken", mock.Anything, user.ScopeAuthentication, tc.token).Return(tc.usr, nil)
			mockUsrService.On("GetUserPermissions", mock.Anything, tc.usr.ID).Return(tc.usrPerms, nil)
			mw := &Middleware{
				config:      nil,
				log:         log,
				helper:      helper,
				userService: &mockUsrService,
			}

			next := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				_, err := w.Write([]byte("User allowed"))
				if err != nil {
					t.Error(err)
				}
			})

			mw.Authenticate(mw.RequirePermission("write", next)).ServeHTTP(rr, r)
			rs := rr.Result()

			defer rs.Body.Close()
			body, err := io.ReadAll(rs.Body)
			if err != nil {
				t.Error(err)
			}
			assert.Equal(t, tc.expectedBody, string(body))
		})
	}
}
