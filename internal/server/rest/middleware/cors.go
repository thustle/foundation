package middleware

import (
	"net/http"
	"strings"
)

func (m *Middleware) EnableCORS(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Vary", "Origin")
		w.Header().Add("Vary", "Access-Control-Request-Method")

		origin := r.Header.Get("Origin")
		if origin != "" && m.config.TrustedOrigins != "" {
			for _, trusted := range strings.Split(m.config.TrustedOrigins, " ") {
				if origin == trusted {
					w.Header().Set("Access-Control-Allow-Origin", trusted)
					// Check if the request is OPTIONS and contains "Access-Control-Request-Method" header.
					// If it does, then we treat it as a preflight request.
					if r.Method == http.MethodOptions && r.Header.Get("Access-Control-Request-Method") != "" {
						w.Header().Set("Access-Control-Allow-Methods", "OPTIONS, PUT, PATCH, DELETE")
						w.Header().Set("Access-Control-Allow-Headers", "Authorization, Content-Type")
						w.WriteHeader(http.StatusOK)
						return
					}
					break
				}
			}
		}
		next.ServeHTTP(w, r)
	})
}
