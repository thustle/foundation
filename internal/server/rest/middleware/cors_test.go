package middleware

import (
	config2 "foundation/config"
	"github.com/stretchr/testify/assert"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestCORSGet(t *testing.T) {
	rr := httptest.NewRecorder()
	r, err := http.NewRequest(http.MethodGet, "/", nil)
	if err != nil {
		t.Fatal(err)
	}
	r.Header.Set("Origin", "123")
	config := &config2.Config{
		CORS: config2.CORS{
			TrustedOrigins: "ABC 123",
		},
	}
	mw := &Middleware{config: config}

	next := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte("Response"))
		if err != nil {
			t.Error(err)
		}
	})

	mw.EnableCORS(next).ServeHTTP(rr, r)
	rs := rr.Result()

	defer rs.Body.Close()
	body, err := io.ReadAll(rs.Body)
	if err != nil {
		t.Error(err)
	}
	assert.Equal(t, "Response", string(body))

	allowOrigin := rs.Header.Get("Access-Control-Allow-Origin")
	assert.Equal(t, "123", allowOrigin)
}

func TestCORSOptions(t *testing.T) {
	rr := httptest.NewRecorder()
	r, err := http.NewRequest(http.MethodOptions, "/", nil)
	if err != nil {
		t.Fatal(err)
	}
	r.Header.Set("Origin", "123")
	r.Header.Set("Access-Control-Request-Method", "OPTIONS")
	config := &config2.Config{
		CORS: config2.CORS{
			TrustedOrigins: "ABC 123",
		},
	}
	mw := Middleware{config: config}

	next := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte("Response"))
		if err != nil {
			t.Error(err)
		}
	})

	mw.EnableCORS(next).ServeHTTP(rr, r)
	rs := rr.Result()

	assert.Equal(t, http.StatusOK, rs.StatusCode)
	allowOrigin := rs.Header.Get("Access-Control-Allow-Origin")
	assert.Equal(t, "123", allowOrigin)
	allowMethods := rs.Header.Get("Access-Control-Allow-Methods")
	assert.Equal(t, "OPTIONS, PUT, PATCH, DELETE", allowMethods)
	allowHeaders := rs.Header.Get("Access-Control-Allow-Methods")
	assert.Equal(t, "OPTIONS, PUT, PATCH, DELETE", allowHeaders)
}
