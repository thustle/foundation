package middleware

import (
	"errors"
	"foundation/internal/domain/context"
	error2 "foundation/internal/domain/error"
	"foundation/internal/domain/user"
	"foundation/internal/validator"
	"net/http"
	"strings"
)

type AuthMiddleware interface {
	RequirePermission(code string, next http.HandlerFunc) http.HandlerFunc
	RequireAuthenticatedUser(next http.HandlerFunc) http.HandlerFunc
}

func (m *Middleware) contextSetUser(r *http.Request, user *user.User) *http.Request {
	return r.WithContext(context.SetUser(r.Context(), user))
}

func (m *Middleware) contextGetUser(r *http.Request) *user.User {
	usr, ok := context.GetUser(r.Context())
	if !ok {
		m.log.Error("missing user value in request context")
	}
	return usr
}

func (m *Middleware) Authenticate(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Vary", "Authorization")
		authHeader := r.Header.Get("Authorization")
		if authHeader == "" {
			r = m.contextSetUser(r, user.AnonymousUser)
			next.ServeHTTP(w, r)
			return
		}

		headerParts := strings.Split(authHeader, " ")
		if len(headerParts) != 2 || headerParts[0] != "Bearer" {
			m.helper.InvalidAuthenticationTokenResponse(w, r)
			return
		}
		token := headerParts[1]

		v := validator.New()
		if user.ValidateTokenPlaintext(v, token); !v.Valid() {
			m.helper.InvalidAuthenticationTokenResponse(w, r)
			return
		}

		usr, err := m.userService.GetByToken(r.Context(), user.ScopeAuthentication, token)
		if err != nil {
			switch {
			case errors.Is(err, error2.ErrRecordNotFound):
				m.helper.InvalidAuthenticationTokenResponse(w, r)
			default:
				m.helper.ServerErrorResponse(w, r, err)
			}
			return
		}

		r = m.contextSetUser(r, usr)
		next.ServeHTTP(w, r)
	})
}

func (m *Middleware) RequireAuthenticatedUser(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		user := m.contextGetUser(r)
		if user.IsAnonymous() {
			m.helper.AuthenticationRequiredResponse(w, r)
			return
		}
		next.ServeHTTP(w, r)
	}
}

func (m *Middleware) RequireActivatedUser(next http.HandlerFunc) http.HandlerFunc {
	fn := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		usr := m.contextGetUser(r)
		if !usr.Activated {
			m.helper.InactiveAccountResponse(w, r)
			return
		}
		next.ServeHTTP(w, r)
	})
	return m.RequireAuthenticatedUser(fn)
}

func (m *Middleware) RequirePermission(code string, next http.HandlerFunc) http.HandlerFunc {
	fn := func(w http.ResponseWriter, r *http.Request) {
		usr := m.contextGetUser(r)
		permissions, err := m.userService.GetUserPermissions(r.Context(), usr.ID)
		if err != nil {
			m.helper.ServerErrorResponse(w, r, err)
			return
		}
		if !permissions.Includes(code) {
			m.helper.NotPermittedResponse(w, r)
			return
		}
		next.ServeHTTP(w, r)
	}
	return m.RequireActivatedUser(fn)
}
