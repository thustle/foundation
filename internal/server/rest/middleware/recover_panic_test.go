package middleware

import (
	"bytes"
	"foundation/internal/server/rest/util"
	"github.com/stretchr/testify/assert"
	"io"
	"log/slog"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestRecoverPanic(t *testing.T) {
	rr := httptest.NewRecorder()
	r, err := http.NewRequest(http.MethodGet, "/", nil)
	if err != nil {
		t.Fatal(err)
	}
	var buff bytes.Buffer
	log := slog.New(slog.NewTextHandler(&buff, nil))
	helper := util.NewHelper(log, nil)
	mw := Middleware{log: log, helper: helper}

	next := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		panic("Unexpected panic")
	})

	mw.RecoverPanic(next).ServeHTTP(rr, r)
	rs := rr.Result()

	defer rs.Body.Close()
	body, err := io.ReadAll(rs.Body)
	if err != nil {
		t.Error(err)
	}
	assert.Equal(t, http.StatusInternalServerError, rs.StatusCode)
	assert.Contains(t, string(body), "could not process your request")
	logBody := string(buff.Bytes())
	assert.Contains(t, logBody, "Unexpected panic")
}
