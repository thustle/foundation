package middleware

import (
	"foundation/config"
	"foundation/internal/domain/user"
	"foundation/internal/server/rest/util"
	"log/slog"
	"sync"
)

type Middleware struct {
	config      *config.Config
	log         *slog.Logger
	helper      *util.Helper
	userService user.Service
}

var middlewareOnce sync.Once
var middleware *Middleware

func NewMiddleware(cfg *config.Config, log *slog.Logger, restHelper *util.Helper, userService user.Service) *Middleware {
	middlewareOnce.Do(func() {
		middleware = &Middleware{
			config:      cfg,
			log:         log,
			helper:      restHelper,
			userService: userService,
		}
	})
	return middleware
}
