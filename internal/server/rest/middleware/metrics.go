package middleware

import (
	"expvar"
	"foundation/internal/server/rest/metrics"
	"net/http"
	"strconv"
	"time"
)

var (
	totalRequestsReceived           = expvar.NewInt("total_requests_received")
	totalResponsesSent              = expvar.NewInt("total_responses_sent")
	totalProcessingTimeMicroseconds = expvar.NewInt("total_processing_time_μs")
	// Declare a new expvar map to hold the count of responses for each HTTP status code.
	totalResponsesSentByStatus = expvar.NewMap("total_responses_sent_by_status")
)

func (m *Middleware) Metrics(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		// Create a new metricsResponseWriter, which wraps the original http.ResponseWriter and adds metrics.
		mw := metrics.NewMetricsResponseWriter(w)

		totalRequestsReceived.Add(1)
		next.ServeHTTP(mw, r)
		totalResponsesSent.Add(1)

		// At this point, the response status code should be stored in the mw.StatusCode field
		totalResponsesSentByStatus.Add(strconv.Itoa(mw.StatusCode), 1)

		duration := time.Since(start).Microseconds()
		totalProcessingTimeMicroseconds.Add(duration)
	})
}
