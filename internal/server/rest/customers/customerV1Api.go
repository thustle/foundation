package customers

import (
	"foundation/config"
	"foundation/internal/domain/customer"
	"foundation/internal/server/rest/middleware"
	"foundation/internal/server/rest/util"
	"log/slog"
	"net/http"
	"sync"
)

type V1Api struct {
	customerUseCase     customer.Service
	config              *config.Config
	log                 *slog.Logger
	restHelper          *util.Helper
	backgroundWaitGroup *sync.WaitGroup
	middleware          middleware.AuthMiddleware
}

var userV1Once sync.Once
var customerV1Api *V1Api

func NewV1Api(customerUseCase customer.Service, cfg *config.Config, log *slog.Logger, helper *util.Helper, wg *sync.WaitGroup, mw middleware.AuthMiddleware) *V1Api {
	userV1Once.Do(func() {
		customerV1Api = &V1Api{
			customerUseCase:     customerUseCase,
			config:              cfg,
			log:                 log,
			restHelper:          helper,
			backgroundWaitGroup: wg,
			middleware:          mw,
		}
	})
	return customerV1Api
}

func (api *V1Api) ConfigureRoutes(mux *http.ServeMux, handler func(http.Handler) http.Handler) {
	mux.Handle("POST /v1/customers", handler(api.middleware.RequirePermission("customers:write", api.createCustomerHandler)))
	mux.Handle("GET /v1/customers", handler(api.middleware.RequirePermission("customers:read", api.getCustomersHandler)))
	mux.Handle("GET /v1/customers/{id}", handler(api.middleware.RequirePermission("customers:read", api.getCustomerHandler)))
}
