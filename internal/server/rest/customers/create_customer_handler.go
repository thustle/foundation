package customers

import (
	"errors"
	"foundation/internal/domain/customer"
	"foundation/internal/server/rest/util"
	"foundation/internal/validator"
	"net/http"
)

// swagger:parameters createCustomer
type CreateCustomerRequestWrapper struct {
	// in: body
	// required: true
	CreateCustomerRequest CreateCustomerRequest
}

type CreateCustomerRequest struct {
	Forename string                  `json:"forename"`
	Surname  string                  `json:"surname"`
	DOB      *customer.JsonBirthDate `json:"dob"`
	Email    *string                 `json:"email"`
	Phone    *string                 `json:"phone"`
}

// createCustomerHandler swagger:route POST /v1/customers CustomersV1 createCustomer
//
// Creates a new customer on the system.
//
//		Consumes:
//		- application/json
//
//		Produces:
//		- application/json
//
//		Schemes: http, https
//
//	 Security:
//	   Bearer:
//
//		Responses:
//		  201: customerResponse
//		  400: validationErrorResponse
func (api *V1Api) createCustomerHandler(w http.ResponseWriter, r *http.Request) {
	var input CreateCustomerRequest
	err := util.ReadJSON(w, r, &input)
	if err != nil {
		api.restHelper.BadRequestResponse(w, r, err)
		return
	}

	customerEntity := &customer.Customer{
		Forename: input.Forename,
		Surname:  input.Surname,
		DOB:      input.DOB,
		Email:    input.Email,
		Phone:    input.Phone,
	}

	err = api.customerUseCase.Create(r.Context(), customerEntity)
	if err != nil {
		var validationErr *validator.ValidationError
		if ok := errors.As(err, &validationErr); ok {
			api.log.Error("CustomerV1Api - createCustomerHandler - userUseCase.Create", "err", err)
			api.restHelper.FailedValidationResponse(w, r, validationErr.Errors)
		} else {
			api.log.Error("CustomerV1Api - createCustomerHandler - userUseCase.Create", "err", err)
			api.restHelper.ServerErrorResponse(w, r, err)
		}
		return
	}

	err = util.WriteJSON(w, http.StatusCreated, fromCustomer(customerEntity), nil)
	if err != nil {
		api.restHelper.ServerErrorResponse(w, r, err)
	}
}
