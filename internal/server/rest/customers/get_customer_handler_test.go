package customers

import (
	"foundation/internal/domain/customer"
	error2 "foundation/internal/domain/error"
	"foundation/internal/server/rest/util"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"log/slog"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"
)

func TestGetCustomerHandler(t *testing.T) {
	c := customer.Customer{ID: 5}
	testCases := []struct {
		name           string
		serviceErr     error
		customer       *customer.Customer
		expectedStatus int
	}{
		{
			name:           "Got Customer -> OK",
			serviceErr:     nil,
			customer:       &c,
			expectedStatus: http.StatusOK,
		},
		{
			name:           "Not Found -> Not Found Error",
			serviceErr:     error2.ErrRecordNotFound,
			customer:       nil,
			expectedStatus: http.StatusNotFound,
		},
	}
	log := slog.Default()
	wg := sync.WaitGroup{}
	helper := util.NewHelper(log, &wg)

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {

			rr := httptest.NewRecorder()
			routePath := "/v1/customers/5"
			req, err := http.NewRequest(http.MethodGet, routePath, nil)
			req.SetPathValue("id", "5")
			if err != nil {
				t.Fatal(err)
			}
			mockService := mockCustomerService{}
			mockMiddleware := mockAuthMiddleware{}
			mockService.On("GetByID", mock.Anything, mock.AnythingOfType("int64")).
				Return(tc.customer, tc.serviceErr).
				Once()

			api := &V1Api{&mockService, nil, log, helper, &wg, &mockMiddleware}
			api.getCustomerHandler(rr, req)

			assert.Equal(t, tc.expectedStatus, rr.Code)
		})
	}
}

func TestGetCustomerHandlerBadRequestData(t *testing.T) {
	log := slog.Default()
	wg := sync.WaitGroup{}
	helper := util.NewHelper(log, &wg)

	rr := httptest.NewRecorder()
	routePath := "/v1/customers/invalid"
	req, err := http.NewRequest(http.MethodGet, routePath, nil)
	req.SetPathValue("id", "invalid")
	if err != nil {
		t.Fatal(err)
	}
	mockService := mockCustomerService{}
	mockMiddleware := mockAuthMiddleware{}
	api := &V1Api{&mockService, nil, log, helper, &wg, &mockMiddleware}
	api.getCustomerHandler(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
}
