package customers

import (
	"context"
	"foundation/internal/domain/customer"
	"foundation/internal/domain/data"
	"github.com/stretchr/testify/mock"
	"net/http"
)

type mockCustomerService struct{ mock.Mock }

func (m *mockCustomerService) Create(ctx context.Context, customer *customer.Customer) error {
	args := m.Called(ctx, customer)
	return args.Error(0)
}

func (m *mockCustomerService) Search(ctx context.Context, req customer.SearchRequest) ([]*customer.Customer, data.Metadata, error) {
	args := m.Called(ctx, req)
	arg0 := args.Get(0)
	if arg0 != nil {
		return arg0.([]*customer.Customer), data.Metadata{}, args.Error(2)
	} else {
		return nil, data.Metadata{}, args.Error(2)
	}
}

func (m *mockCustomerService) GetByID(ctx context.Context, id int64) (*customer.Customer, error) {
	args := m.Called(ctx, id)
	arg0 := args.Get(0)
	if arg0 != nil {
		return arg0.(*customer.Customer), args.Error(1)
	} else {
		return nil, args.Error(1)
	}
}

type mockAuthMiddleware struct{ mock.Mock }

func (m *mockAuthMiddleware) RequirePermission(_ string, next http.HandlerFunc) http.HandlerFunc {
	return next.ServeHTTP
}

func (m *mockAuthMiddleware) RequireAuthenticatedUser(next http.HandlerFunc) http.HandlerFunc {
	return next.ServeHTTP
}
