package customers

import (
	"bytes"
	"encoding/json"
	"errors"
	"foundation/internal/server/rest/util"
	"foundation/internal/validator"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"log/slog"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"
)

func TestCreateCustomerHandler(t *testing.T) {
	testCases := []struct {
		name           string
		tokenErr       error
		expectedStatus int
	}{
		{
			name:           "Customer created -> OK",
			tokenErr:       nil,
			expectedStatus: http.StatusCreated,
		},
		{
			name:           "Validation Err -> Bad Request",
			tokenErr:       &validator.ValidationError{},
			expectedStatus: http.StatusBadRequest,
		},
		{
			name:           "Other Err -> Internal Server Error",
			tokenErr:       errors.New("new error"),
			expectedStatus: http.StatusInternalServerError,
		},
	}
	log := slog.Default()
	wg := sync.WaitGroup{}
	helper := util.NewHelper(log, &wg)
	requestObject := &CreateCustomerRequest{Forename: "a", Surname: "surname"}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {

			rr := httptest.NewRecorder()
			routePath := "/v1/users"
			body, _ := json.Marshal(requestObject)
			req, err := http.NewRequest(http.MethodPost, routePath, bytes.NewReader(body))
			if err != nil {
				t.Fatal(err)
			}
			mockService := mockCustomerService{}
			mockMiddleware := mockAuthMiddleware{}
			mockService.On("Create", mock.Anything, mock.AnythingOfType("*customer.Customer")).
				Return(tc.tokenErr).
				Once()

			api := &V1Api{&mockService, nil, log, helper, &wg, &mockMiddleware}
			api.createCustomerHandler(rr, req)

			assert.Equal(t, tc.expectedStatus, rr.Code)
		})
	}
}

func TestCreateCustomerHandlerBadRequestData(t *testing.T) {
	log := slog.Default()
	wg := sync.WaitGroup{}
	helper := util.NewHelper(log, &wg)

	rr := httptest.NewRecorder()
	routePath := "/v1/users"
	body := []byte(`{"email:":invalid,"forename":""}`)
	req, err := http.NewRequest(http.MethodPost, routePath, bytes.NewReader(body))
	if err != nil {
		t.Fatal(err)
	}
	mockService := mockCustomerService{}
	mockMiddleware := mockAuthMiddleware{}
	api := &V1Api{&mockService, nil, log, helper, &wg, &mockMiddleware}
	api.createCustomerHandler(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
}
