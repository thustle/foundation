package customers

import (
	"errors"
	"foundation/internal/domain/customer"
	"foundation/internal/domain/data"
	"foundation/internal/server/rest/util"
	"foundation/internal/validator"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"log/slog"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"
)

func TestGetCustomersHandler(t *testing.T) {
	testCases := []struct {
		name           string
		serviceErr     error
		customers      []*customer.Customer
		metadata       data.Metadata
		expectedStatus int
	}{
		{
			name:           "Customer Search -> OK",
			serviceErr:     nil,
			customers:      make([]*customer.Customer, 0),
			metadata:       data.Metadata{PageSize: 20, CurrentPage: 0},
			expectedStatus: http.StatusOK,
		},
		{
			name:           "Validation Err -> Bad Request",
			serviceErr:     &validator.ValidationError{},
			customers:      nil,
			metadata:       data.Metadata{},
			expectedStatus: http.StatusBadRequest,
		},
		{
			name:           "Other Err -> Internal Server Error",
			serviceErr:     errors.New("new error"),
			customers:      nil,
			metadata:       data.Metadata{},
			expectedStatus: http.StatusInternalServerError,
		},
	}
	log := slog.Default()
	wg := sync.WaitGroup{}
	helper := util.NewHelper(log, &wg)

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {

			rr := httptest.NewRecorder()
			routePath := "/v1/customers?surname=xyz"
			req, err := http.NewRequest(http.MethodGet, routePath, nil)
			if err != nil {
				t.Fatal(err)
			}
			mockService := mockCustomerService{}
			mockMiddleware := mockAuthMiddleware{}
			mockService.On("Search", mock.Anything, mock.AnythingOfType("customer.SearchRequest")).
				Return(tc.customers, tc.metadata, tc.serviceErr).
				Once()

			api := &V1Api{&mockService, nil, log, helper, &wg, &mockMiddleware}
			api.getCustomersHandler(rr, req)

			assert.Equal(t, tc.expectedStatus, rr.Code)
		})
	}
}

func TestGetCustomersHandlerBadRequestData(t *testing.T) {
	log := slog.Default()
	wg := sync.WaitGroup{}
	helper := util.NewHelper(log, &wg)

	rr := httptest.NewRecorder()
	routePath := "/v1/customers?sort=invalid"
	req, err := http.NewRequest(http.MethodGet, routePath, nil)
	if err != nil {
		t.Fatal(err)
	}
	mockService := mockCustomerService{}
	mockMiddleware := mockAuthMiddleware{}
	api := &V1Api{&mockService, nil, log, helper, &wg, &mockMiddleware}
	api.getCustomersHandler(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
}
