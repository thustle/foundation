package customers

import (
	"errors"
	"foundation/internal/domain/customer"
	"foundation/internal/domain/data"
	"foundation/internal/server/rest/util"
	"foundation/internal/validator"
	"net/http"
)

// getCustomersHandler swagger:route GET /v1/customers CustomersV1 getCustomers
//
// Searches for customers in the system.
//
//				Consumes:
//				- application/json
//
//				Produces:
//				- application/json
//
//				Schemes: http, https
//
//			 Security:
//			   Bearer:
//
//		    Parameters:
//		      + name: forename
//		        in: query
//		        description: forename to search for
//		        required: false
//		        type: string
//		      + name: surname
//		        in: query
//		        description: surname to search for
//		        required: false
//		        type: string
//		      + name: email
//		        in: query
//		        description: email to search for
//		        required: false
//		        type: string
//		      + name: phone
//		        in: query
//		        description: phone to search for
//		        required: false
//		        type: string
//		      + name: page
//		        in: query
//		        description: which page to retrieve
//		        required: false
//		        type: integer
//	         format: int32
//		      + name: page_size
//		        in: query
//		        description: size of the page to retrieve
//		        required: false
//		        type: integer
//	         format: int32
//		      + name: sort
//		        in: query
//		        description: Sort-by fields (e.g. "-forename,dob")
//		        required: false
//		        type: string
//
//				Responses:
//				  200: customersResponse
//				  400: validationErrorResponse
func (api *V1Api) getCustomersHandler(w http.ResponseWriter, r *http.Request) {
	var input customer.SearchRequest
	v := validator.New()
	qs := r.URL.Query()

	input.Forename = util.ReadString(qs, "forename", "")
	input.Surname = util.ReadString(qs, "surname", "")
	input.Email = util.ReadString(qs, "email", "")
	input.Phone = util.ReadString(qs, "phone", "")
	input.Page = util.ReadInt(qs, "page", 1, v)
	input.PageSize = util.ReadInt(qs, "page_size", 20, v)
	input.Sort = util.ReadString(qs, "sort", "id")
	input.SortSafelist = []string{"id", "forename", "surname", "dob", "email", "phone", "-id", "-forename", "-surname", "-dob", "-email", "-phone"}
	if data.ValidateFilters(v, input.Filters); !v.Valid() {
		api.restHelper.FailedValidationResponse(w, r, v.Errors)
		return
	}

	customers, metadata, err := api.customerUseCase.Search(r.Context(), input)
	if err != nil {
		var validationErr *validator.ValidationError
		if ok := errors.As(err, &validationErr); ok {
			api.log.Error("CustomerV1Api - searchCustomersHandler - customerUseCase.Search", "err", err)
			api.restHelper.FailedValidationResponse(w, r, validationErr.Errors)
		} else {
			api.restHelper.ServerErrorResponse(w, r, err)
		}
		return
	}

	err = util.WriteJSON(w, http.StatusOK, fromCustomers(customers, metadata), nil)
	if err != nil {
		api.restHelper.ServerErrorResponse(w, r, err)
	}
}
