package customers

import (
	"errors"
	error2 "foundation/internal/domain/error"
	"foundation/internal/server/rest/util"
	"foundation/internal/validator"
	"net/http"
	"strconv"
)

// getCustomerHandler swagger:route GET /v1/customers/{id} CustomersV1 getCustomerById
//
// Searches for customers in the system.
//
//			Consumes:
//			- application/json
//
//			Produces:
//			- application/json
//
//			Schemes: http, https
//
//		 Security:
//		   Bearer:
//
//	    Parameters:
//	      + name: id
//	        in: path
//	        description: ID of the customer
//	        required: true
//	        type: int64
//
//			Responses:
//			  200: customerResponse
//			  400: validationErrorResponse
func (api *V1Api) getCustomerHandler(w http.ResponseWriter, r *http.Request) {
	v := validator.New()
	idStr := r.PathValue("id")
	id, err := strconv.ParseInt(idStr, 10, 64)
	if err != nil {
		v.AddError("id", "Invalid id value")
		api.restHelper.FailedValidationResponse(w, r, v.Errors)
		return
	}
	cust, err := api.customerUseCase.GetByID(r.Context(), id)
	if err != nil {
		if ok := errors.Is(err, error2.ErrRecordNotFound); ok {
			api.log.Error("CustomerV1Api - getCustomerHandler - customerUseCase.GetByID", "err", err)
			api.restHelper.NotFoundResponse(w, r)
		} else {
			api.restHelper.ServerErrorResponse(w, r, err)
		}
		return
	}

	err = util.WriteJSON(w, http.StatusOK, fromCustomer(cust), nil)
	if err != nil {
		api.restHelper.ServerErrorResponse(w, r, err)
	}
}
