package customers

import (
	"foundation/internal/domain/customer"
	"foundation/internal/domain/data"
	"time"
)

// swagger:response customersResponse
type CustomersResponseWrapper struct {
	CustomersResponse CustomersResponse
}

type CustomersResponse struct {
	Customers []CustomerResponse `json:"customers"`
	Metadata  data.Metadata      `json:"metadata"`
}

func fromCustomers(c []*customer.Customer, metadata data.Metadata) *CustomersResponse {
	customers := make([]CustomerResponse, len(c))
	for i, cust := range c {
		customers[i] = *fromCustomer(cust)
	}
	return &CustomersResponse{Customers: customers, Metadata: metadata}
}

// swagger:response customerResponse
type CustomerResponseWrapper struct {
	CustomerResponse CustomerResponse
}

type CustomerResponse struct {
	ID        int64                   `json:"id"`
	CreatedAt time.Time               `json:"created_at"`
	Forename  string                  `json:"forename"`
	Surname   string                  `json:"surname"`
	DOB       *customer.JsonBirthDate `json:"dob"`
	Email     *string                 `json:"email"`
	Phone     *string                 `json:"phone"`
}

func fromCustomer(c *customer.Customer) *CustomerResponse {
	return &CustomerResponse{
		ID:        c.ID,
		CreatedAt: c.CreatedAt,
		Forename:  c.Forename,
		Surname:   c.Surname,
		DOB:       c.DOB,
		Email:     c.Email,
		Phone:     c.Phone,
	}
}
