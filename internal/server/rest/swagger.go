// Package rest Foundation API.
//
// the purpose of this application is to provide a foundation application
// that is using plain go code to define an API
//
// This allows user authentication, metrics, healthcheck and customer endpoints.
//
//		Schemes: http
//		Host: localhost:8080
//		BasePath: /
//		Version: 0.0.1
//		License: MIT http://opensource.org/licenses/MIT
//
//		Consumes:
//		- application/json
//
//		Produces:
//		- application/json
//
//		SecurityDefinitions:
//	      Bearer:
//	        type: apiKey
//	        name: Authorization
//	        in: header
//	        description: >-
//	          Enter the token with the `Bearer: ` prefix, e.g. "Bearer abcde12345".
//
// swagger:meta
package rest
