//go:generate swagger generate spec -o ./swagger-ui/swagger.yaml

package rest

import (
	"context"
	"embed"
	"foundation/config"
	"foundation/internal/domain"
	"foundation/internal/server/rest/customers"
	"foundation/internal/server/rest/healthcheck"
	"foundation/internal/server/rest/metrics"
	"foundation/internal/server/rest/middleware"
	"foundation/internal/server/rest/users"
	"foundation/internal/server/rest/util"
	"io/fs"
	"log/slog"
	"net"
	"net/http"
	"sync"
	"time"
)

// Server -.
type Server struct {
	server              *http.Server
	log                 *slog.Logger
	helper              *util.Helper
	notify              chan error
	Router              http.Handler
	shutdownTimeout     time.Duration
	BackgroundWaitGroup *sync.WaitGroup
}

func New(cfg *config.Config, log *slog.Logger, helper *util.Helper, services *domain.Services, wg *sync.WaitGroup) *Server {
	server := prepareHttpServer(cfg, log, helper, services, wg)
	return server
}

func prepareHttpServer(cfg *config.Config, log *slog.Logger, helper *util.Helper, services *domain.Services, wg *sync.WaitGroup) *Server {
	mux := http.NewServeMux()
	mw := middleware.NewMiddleware(cfg, log, helper, services.UserService)
	baseRouter := func(h http.Handler) http.Handler {
		return mw.Metrics(mw.RecoverPanic(mw.EnableCORS(mw.RateLimit(mw.Authenticate(h)))))
	}

	routeSources := createRouteSources(cfg, log, helper, services, wg, mw)
	for _, r := range routeSources {
		r.ConfigureRoutes(mux, baseRouter)
	}

	httpServer := &http.Server{
		Handler:      mux,
		ReadTimeout:  time.Duration(cfg.ReadTimeout) * time.Second,
		WriteTimeout: time.Duration(cfg.WriteTimeout) * time.Second,
	}
	httpServer.Addr = net.JoinHostPort("localhost", cfg.HTTP.Port)
	hostSwaggerUI(mux)

	s := &Server{
		server:              httpServer,
		log:                 log,
		helper:              helper,
		notify:              make(chan error, 1),
		Router:              mux,
		shutdownTimeout:     time.Second * time.Duration(cfg.ShutdownTimeout),
		BackgroundWaitGroup: wg,
	}
	return s
}

func (s *Server) Start() {
	go func() {
		err := s.server.ListenAndServe()
		s.log.Info("Server stopping", "err", err)
		s.notify <- err
		close(s.notify)
	}()
}

// Notify -.
func (s *Server) Notify() <-chan error {
	return s.notify
}

// Shutdown -.
func (s *Server) Shutdown() error {
	ctx, cancel := context.WithTimeout(context.Background(), s.shutdownTimeout)
	defer cancel()

	return s.server.Shutdown(ctx)
}

func createRouteSources(cfg *config.Config, log *slog.Logger, helper *util.Helper, services *domain.Services, wg *sync.WaitGroup, mw *middleware.Middleware) []RouteSource {
	return []RouteSource{
		users.NewUserV1Api(services.UserService, cfg, log, helper, wg),
		healthcheck.NewHealthCheckV1API(cfg, helper),
		metrics.NewMetricsV1API(),
		customers.NewV1Api(services.CustomerService, cfg, log, helper, wg, mw),
	}
}

//go:embed swagger-ui
var swaggerUI embed.FS

func hostSwaggerUI(mux *http.ServeMux) {
	fSys, err := fs.Sub(swaggerUI, "swagger-ui")
	if err != nil {
		panic(err)
	}
	swaggerFS := http.FileServer(http.FS(fSys))
	mux.Handle("/swagger-ui/", http.StripPrefix("/swagger-ui/", swaggerFS))
}
