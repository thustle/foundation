package rest

import "net/http"

type RouteSource interface {
	ConfigureRoutes(*http.ServeMux, func(http.Handler) http.Handler)
}
