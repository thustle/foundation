package users

import (
	"bytes"
	"encoding/json"
	"errors"
	error2 "foundation/internal/domain/error"
	"foundation/internal/server/rest/util"
	"foundation/internal/validator"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"log/slog"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"
)

func TestCreateActivationTokenHandler(t *testing.T) {
	testCases := []struct {
		name           string
		tokenErr       error
		expectedStatus int
	}{
		{
			name:           "Token returned -> OK",
			tokenErr:       nil,
			expectedStatus: http.StatusAccepted,
		},
		{
			name:           "Conflict Error -> Conflict",
			tokenErr:       error2.ErrEditConflict,
			expectedStatus: http.StatusConflict,
		},
		{
			name:           "Validation Err -> Bad Request",
			tokenErr:       &validator.ValidationError{},
			expectedStatus: http.StatusBadRequest,
		},
		{
			name:           "Other Err -> Internal Server Error",
			tokenErr:       errors.New("new error"),
			expectedStatus: http.StatusInternalServerError,
		},
	}
	log := slog.Default()
	wg := sync.WaitGroup{}
	helper := util.NewHelper(log, &wg)
	requestObject := &CreateActivationTokenRequest{Email: "user@company.com"}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {

			rr := httptest.NewRecorder()
			routePath := "/v1/users/activation"
			body, _ := json.Marshal(requestObject)
			req, err := http.NewRequest(http.MethodPost, routePath, bytes.NewReader(body))
			if err != nil {
				t.Fatal(err)
			}
			mockUsrService := mockUserService{}
			mockUsrService.On("CreateActivationToken", mock.Anything, requestObject.Email).
				Return(tc.tokenErr).
				Once()

			api := &UserV1Api{&mockUsrService, nil, log, helper, &wg}
			api.createActivationTokenHandler(rr, req)

			assert.Equal(t, tc.expectedStatus, rr.Code)
		})
	}
}

func TestCreateActivationTokenHandlerBadRequestData(t *testing.T) {
	log := slog.Default()
	wg := sync.WaitGroup{}
	helper := util.NewHelper(log, &wg)

	rr := httptest.NewRecorder()
	routePath := "/v1/users/activation"
	body := []byte(`{"email:":invalid}`)
	req, err := http.NewRequest(http.MethodPost, routePath, bytes.NewReader(body))
	if err != nil {
		t.Fatal(err)
	}
	mockUsrService := mockUserService{}
	api := &UserV1Api{&mockUsrService, nil, log, helper, &wg}
	api.createActivationTokenHandler(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
}
