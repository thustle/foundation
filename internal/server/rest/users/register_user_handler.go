package users

import (
	"errors"
	"foundation/internal/domain/user"
	"foundation/internal/server/rest/util"
	"foundation/internal/validator"
	"net/http"
)

// swagger:parameters registerUser
type UserRequestWrapper struct {
	// in: body
	// required: true
	// example: {"name:"John","email":"john@company.com","password":"password123"}
	UserRequest UserRequest
}

type UserRequest struct {
	Name     string `json:"name"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

// registerUserHandler swagger:route POST /v1/users UsersV1 registerUser
//
// Registers the user on the system.
//
// User should provide their details in order to register.
//
//	Consumes:
//	- application/json
//
//	Produces:
//	- application/json
//
//	Schemes: http, https
//
//	Responses:
//	  201: userResponse
//	  400: validationErrorResponse
func (api *UserV1Api) registerUserHandler(w http.ResponseWriter, r *http.Request) {
	var input UserRequest
	err := util.ReadJSON(w, r, &input)
	if err != nil {
		api.restHelper.BadRequestResponse(w, r, err)
		return
	}

	userEntity := &user.User{
		Name:      input.Name,
		Email:     input.Email,
		Activated: false,
	}
	err = userEntity.Password.Set(input.Password)
	if err != nil {
		api.restHelper.ServerErrorResponse(w, r, err)
		return
	}

	err = api.userUseCase.Create(r.Context(), userEntity)
	if err != nil {
		var validationErr *validator.ValidationError
		if ok := errors.As(err, &validationErr); ok {
			api.log.Error("UserV1Api - registerUserHandler - userUseCase.Create", "err", err)
			api.restHelper.FailedValidationResponse(w, r, validationErr.Errors)
		} else {
			api.log.Error("UserV1Api - registerUserHandler - userUseCase.Create", "err", err)
			api.restHelper.ServerErrorResponse(w, r, err)
		}
		return
	}

	err = util.WriteJSON(w, http.StatusCreated, fromUser(userEntity), nil)
	if err != nil {
		api.restHelper.ServerErrorResponse(w, r, err)
	}
}
