package users

import (
	"foundation/internal/domain/user"
	"time"
)

// swagger:response userResponse
type UserResponseWrapper struct {
	UserResponse UserResponse
}

type UserResponse struct {
	ID        int64     `json:"id"`
	CreatedAt time.Time `json:"created_at"`
	Name      string    `json:"name"`
	Email     string    `json:"email"`
	Activated bool      `json:"activated"`
}

func fromUser(usr *user.User) *UserResponse {
	return &UserResponse{
		ID:        usr.ID,
		CreatedAt: usr.CreatedAt,
		Name:      usr.Name,
		Email:     usr.Email,
		Activated: usr.Activated,
	}
}
