package users

import (
	"errors"
	error2 "foundation/internal/domain/error"
	"foundation/internal/domain/user"
	"foundation/internal/server/rest/util"
	"foundation/internal/validator"
	"net/http"
)

// swagger:parameters authRequest
type AuthRequestWrapper struct {
	// in: body
	// required: true
	// example: {"email:"james@company.com","password":"password123"}
	AuthRequest AuthRequest
}

type AuthRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

// swagger:response authResponse
type AuthenticationResponseWrapper struct {
	AuthenticationResponse AuthenticationResponse
}

type AuthenticationResponse struct {
	Token *user.Token `json:"authentication_token"`
}

// authenticationHandler swagger:route POST /v1/users/authentication UsersV1 authRequest
//
// Authenticates the user on the system.
//
// User should provide their email/password details in order to register. They will receive a token to use in subsequent calls.
//
//	Consumes:
//	- application/json
//
//	Produces:
//	- application/json
//
//	Schemes: http, https
//
//	Responses:
//	  201: authResponse
//	  400: validationErrorResponse
//	  401: messageResponse
func (api *UserV1Api) authenticationHandler(w http.ResponseWriter, r *http.Request) {
	var input AuthRequest
	err := util.ReadJSON(w, r, &input)
	if err != nil {
		api.restHelper.BadRequestResponse(w, r, err)
		return
	}

	token, err := api.userUseCase.Authenticate(r.Context(), input.Email, input.Password)
	if err != nil {
		var validationErr *validator.ValidationError
		switch {
		case errors.As(err, &validationErr):
			api.log.Error("UserV1Api - createActivationTokenHandler - userUseCase.createActivationToken", "err", err)
			api.restHelper.FailedValidationResponse(w, r, validationErr.Errors)
		case errors.Is(err, error2.InvalidCredentials):
			api.restHelper.InvalidCredentialsResponse(w, r)
		default:
			api.restHelper.ServerErrorResponse(w, r, err)
		}
		return
	}
	err = util.WriteJSON(w, http.StatusOK, AuthenticationResponse{Token: token}, nil)
	if err != nil {
		api.restHelper.ServerErrorResponse(w, r, err)
	}
}
