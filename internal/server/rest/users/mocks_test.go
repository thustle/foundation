package users

import (
	"context"
	"foundation/internal/domain/user"
	"github.com/stretchr/testify/mock"
)

type mockUserService struct{ mock.Mock }

func (m *mockUserService) GetByEmail(_ context.Context, _ string) (*user.User, error) {
	return nil, nil
}
func (m *mockUserService) Create(ctx context.Context, usr *user.User) error {
	args := m.Called(ctx, usr)
	return args.Error(0)
}
func (m *mockUserService) ActivateUser(ctx context.Context, token string) (*user.User, error) {
	args := m.Called(ctx, token)
	arg0 := args.Get(0)
	if arg0 != nil {
		return arg0.(*user.User), args.Error(1)
	} else {
		return nil, args.Error(1)
	}
}
func (m *mockUserService) CreatePasswordResetToken(ctx context.Context, email string) (*user.Token, error) {
	args := m.Called(ctx, email)
	return nil, args.Error(1)
}
func (m *mockUserService) UpdatePassword(ctx context.Context, password string, token string) error {
	args := m.Called(ctx, password, token)
	return args.Error(0)
}
func (m *mockUserService) CreateActivationToken(ctx context.Context, email string) error {
	args := m.Called(ctx, email)
	return args.Error(0)
}
func (m *mockUserService) Authenticate(ctx context.Context, email string, password string) (*user.Token, error) {
	args := m.Called(ctx, email, password)
	arg0 := args.Get(0)
	if arg0 != nil {
		return arg0.(*user.Token), args.Error(1)
	} else {
		return nil, args.Error(1)
	}
}
func (m *mockUserService) GetUserPermissions(_ context.Context, _ int64) (user.Permissions, error) {
	return nil, nil
}
func (m *mockUserService) GetByToken(_ context.Context, _ string, _ string) (*user.User, error) {
	return nil, nil
}
