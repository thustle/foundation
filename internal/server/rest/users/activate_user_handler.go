package users

import (
	"errors"
	error2 "foundation/internal/domain/error"
	"foundation/internal/server/rest/util"
	"foundation/internal/validator"
	"net/http"
)

// swagger:parameters activateUser
type UserActivationRequestWrapper struct {
	// in: body
	// required: true
	// example: {"token:"ABC123ABC123ABC123ABC123"}
	UserActivationRequest UserActivationRequest
}

type UserActivationRequest struct {
	TokenPlaintext string `json:"token"`
}

// activateUserHandler swagger:route PUT /v1/users/activated UsersV1 activateUser
//
// Activates the user on the system.
//
// User provides their activation token which they received via email to activate their account.
//
//	Consumes:
//	- application/json
//
//	Produces:
//	- application/json
//
//	Schemes: http, https
//
//	Responses:
//	  201: userResponse
//	  400: validationErrorResponse
func (api *UserV1Api) activateUserHandler(w http.ResponseWriter, r *http.Request) {
	var input UserActivationRequest
	err := util.ReadJSON(w, r, &input)
	if err != nil {
		api.restHelper.BadRequestResponse(w, r, err)
		return
	}

	usr, err := api.userUseCase.ActivateUser(r.Context(), input.TokenPlaintext)
	if err != nil {
		var validationErr *validator.ValidationError
		switch {
		case errors.As(err, &validationErr):
			api.log.Error("UserV1Api - activateUserHandler - userUseCase.ActivateUser", "err", err)
			api.restHelper.FailedValidationResponse(w, r, validationErr.Errors)
		case errors.Is(err, error2.ErrEditConflict):
			api.restHelper.EditConflictResponse(w, r)
		default:
			api.restHelper.ServerErrorResponse(w, r, err)
		}
		return
	}

	err = util.WriteJSON(w, http.StatusOK, fromUser(usr), nil)
	if err != nil {
		api.restHelper.ServerErrorResponse(w, r, err)
	}
}
