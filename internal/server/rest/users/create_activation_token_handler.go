package users

import (
	"errors"
	error2 "foundation/internal/domain/error"
	"foundation/internal/server/rest/util"
	"foundation/internal/validator"
	"net/http"
)

// swagger:parameters createActivationToken
type CreateActivationTokenRequestWrapper struct {
	// in: body
	// required: true
	// example: {"email:"james@company.com"}
	CreateActivationTokenRequest CreateActivationTokenRequest
}

type CreateActivationTokenRequest struct {
	Email string `json:"email"`
}

// createActivationTokenHandler swagger:route POST /v1/users/activation UsersV1 createActivationToken
//
// Sends a user activation token to the user.
//
// User will be emailed a user activation token.
//
//	Consumes:
//	- application/json
//
//	Produces:
//	- application/json
//
//	Schemes: http, https
//
//	Responses:
//	  202: messageResponse
//	  400: validationErrorResponse
//	  409: validationErrorResponse
func (api *UserV1Api) createActivationTokenHandler(w http.ResponseWriter, r *http.Request) {
	var input CreateActivationTokenRequest
	err := util.ReadJSON(w, r, &input)
	if err != nil {
		api.restHelper.BadRequestResponse(w, r, err)
		return
	}

	err = api.userUseCase.CreateActivationToken(r.Context(), input.Email)
	if err != nil {
		var validationErr *validator.ValidationError
		switch {
		case errors.As(err, &validationErr):
			api.log.Error("UserV1Api - createActivationTokenHandler - userUseCase.createActivationToken", "err", err)
			api.restHelper.FailedValidationResponse(w, r, validationErr.Errors)
		case errors.Is(err, error2.ErrEditConflict):
			api.restHelper.EditConflictResponse(w, r)
		default:
			api.restHelper.ServerErrorResponse(w, r, err)
		}
		return
	}

	env := util.MessageResponse{Message: "an email will be sent to you containing activation instructions"}
	err = util.WriteJSON(w, http.StatusAccepted, env, nil)
	if err != nil {
		api.restHelper.ServerErrorResponse(w, r, err)
	}
}
