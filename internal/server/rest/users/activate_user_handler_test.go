package users

import (
	"bytes"
	"encoding/json"
	"errors"
	error2 "foundation/internal/domain/error"
	"foundation/internal/domain/user"
	"foundation/internal/server/rest/util"
	"foundation/internal/validator"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"log/slog"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"
)

func TestActivateUserHandler(t *testing.T) {
	testCases := []struct {
		name           string
		usr            *user.User
		tokenErr       error
		expectedStatus int
	}{
		{
			name:           "User returned -> OK",
			usr:            &user.User{Email: "test@test.com"},
			tokenErr:       nil,
			expectedStatus: http.StatusOK,
		},
		{
			name:           "Conflict Err -> Conflict",
			usr:            nil,
			tokenErr:       error2.ErrEditConflict,
			expectedStatus: http.StatusConflict,
		},
		{
			name:           "Validation Err -> Bad Request",
			usr:            nil,
			tokenErr:       &validator.ValidationError{},
			expectedStatus: http.StatusBadRequest,
		},
		{
			name:           "Other Err -> Internal Server Error",
			usr:            nil,
			tokenErr:       errors.New("new error"),
			expectedStatus: http.StatusInternalServerError,
		},
	}
	log := slog.Default()
	wg := sync.WaitGroup{}
	helper := util.NewHelper(log, &wg)
	requestObject := &UserActivationRequest{"ABC123"}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {

			rr := httptest.NewRecorder()
			routePath := "/v1/users/activated"
			body, _ := json.Marshal(requestObject)
			req, err := http.NewRequest(http.MethodPut, routePath, bytes.NewReader(body))
			if err != nil {
				t.Fatal(err)
			}
			mockUsrService := mockUserService{}
			mockUsrService.On("ActivateUser", mock.Anything, requestObject.TokenPlaintext).
				Return(tc.usr, tc.tokenErr).
				Once()

			api := &UserV1Api{&mockUsrService, nil, log, helper, &wg}
			api.activateUserHandler(rr, req)

			assert.Equal(t, tc.expectedStatus, rr.Code)
		})
	}
}

func TestActivateUserHandlerBadRequestData(t *testing.T) {
	log := slog.Default()
	wg := sync.WaitGroup{}
	helper := util.NewHelper(log, &wg)

	rr := httptest.NewRecorder()
	routePath := "/v1/users/activated"
	body := []byte(`{"token:":invalid}`)
	req, err := http.NewRequest(http.MethodPut, routePath, bytes.NewReader(body))
	if err != nil {
		t.Fatal(err)
	}
	mockUsrService := mockUserService{}
	api := &UserV1Api{&mockUsrService, nil, log, helper, &wg}
	api.activateUserHandler(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
}
