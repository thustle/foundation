package users

import (
	"bytes"
	"encoding/json"
	"errors"
	error2 "foundation/internal/domain/error"
	"foundation/internal/server/rest/util"
	"foundation/internal/validator"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"log/slog"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"
)

func TestUpdatePasswordHandler(t *testing.T) {
	testCases := []struct {
		name           string
		tokenErr       error
		expectedStatus int
	}{
		{
			name:           "Token returned -> OK",
			tokenErr:       nil,
			expectedStatus: http.StatusOK,
		},
		{
			name:           "Conflict Err -> Conflict",
			tokenErr:       error2.ErrEditConflict,
			expectedStatus: http.StatusConflict,
		},
		{
			name:           "Validation Err -> Bad Request",
			tokenErr:       &validator.ValidationError{},
			expectedStatus: http.StatusBadRequest,
		},
		{
			name:           "Other Err -> Internal Server Error",
			tokenErr:       errors.New("new error"),
			expectedStatus: http.StatusInternalServerError,
		},
	}
	log := slog.Default()
	wg := sync.WaitGroup{}
	helper := util.NewHelper(log, &wg)
	requestObject := &UpdatePasswordRequest{Password: "pw2", TokenPlaintext: "ABC123"}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {

			rr := httptest.NewRecorder()
			routePath := "/v1/users/password"
			body, _ := json.Marshal(requestObject)
			req, err := http.NewRequest(http.MethodPut, routePath, bytes.NewReader(body))
			if err != nil {
				t.Fatal(err)
			}
			mockUsrService := mockUserService{}
			mockUsrService.On("UpdatePassword", mock.Anything, requestObject.Password, requestObject.TokenPlaintext).
				Return(tc.tokenErr).
				Once()

			api := &UserV1Api{&mockUsrService, nil, log, helper, &wg}
			api.updatePasswordHandler(rr, req)

			assert.Equal(t, tc.expectedStatus, rr.Code)
		})
	}
}

func TestUpdatePasswordHandlerBadRequestData(t *testing.T) {
	log := slog.Default()
	wg := sync.WaitGroup{}
	helper := util.NewHelper(log, &wg)

	rr := httptest.NewRecorder()
	routePath := "/v1/users/password"
	body := []byte(`{"token:":invalid}`)
	req, err := http.NewRequest(http.MethodPut, routePath, bytes.NewReader(body))
	if err != nil {
		t.Fatal(err)
	}
	mockUsrService := mockUserService{}
	api := &UserV1Api{&mockUsrService, nil, log, helper, &wg}
	api.updatePasswordHandler(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
}
