package users

import (
	"bytes"
	"encoding/json"
	"errors"
	"foundation/internal/server/rest/util"
	"foundation/internal/validator"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"log/slog"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"
)

func TestRegisterUserHandler(t *testing.T) {
	testCases := []struct {
		name           string
		tokenErr       error
		expectedStatus int
	}{
		{
			name:           "User created -> OK",
			tokenErr:       nil,
			expectedStatus: http.StatusCreated,
		},
		{
			name:           "Validation Err -> Bad Request",
			tokenErr:       &validator.ValidationError{},
			expectedStatus: http.StatusBadRequest,
		},
		{
			name:           "Other Err -> Internal Server Error",
			tokenErr:       errors.New("new error"),
			expectedStatus: http.StatusInternalServerError,
		},
	}
	log := slog.Default()
	wg := sync.WaitGroup{}
	helper := util.NewHelper(log, &wg)
	requestObject := &UserRequest{Email: "user@company.com", Password: "password123", Name: "User"}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {

			rr := httptest.NewRecorder()
			routePath := "/v1/users"
			body, _ := json.Marshal(requestObject)
			req, err := http.NewRequest(http.MethodPost, routePath, bytes.NewReader(body))
			if err != nil {
				t.Fatal(err)
			}
			mockUsrService := mockUserService{}
			mockUsrService.On("Create", mock.Anything, mock.AnythingOfType("*user.User")).
				Return(tc.tokenErr).
				Once()

			api := &UserV1Api{&mockUsrService, nil, log, helper, &wg}
			api.registerUserHandler(rr, req)

			assert.Equal(t, tc.expectedStatus, rr.Code)
		})
	}
}

func TestRegisterUserHandlerBadRequestData(t *testing.T) {
	log := slog.Default()
	wg := sync.WaitGroup{}
	helper := util.NewHelper(log, &wg)

	rr := httptest.NewRecorder()
	routePath := "/v1/users"
	body := []byte(`{"email:":invalid,"password":"pw"}`)
	req, err := http.NewRequest(http.MethodPost, routePath, bytes.NewReader(body))
	if err != nil {
		t.Fatal(err)
	}
	mockUsrService := mockUserService{}
	api := &UserV1Api{&mockUsrService, nil, log, helper, &wg}
	api.registerUserHandler(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
}
