package users

import (
	"errors"
	error2 "foundation/internal/domain/error"
	"foundation/internal/server/rest/util"
	"foundation/internal/validator"
	"net/http"
)

// swagger:parameters updatePassword
type UpdatePasswordRequestWrapper struct {
	// in: body
	// required: true
	// example: {"password:"new-password","token":"ABC123ABC123"}
	UpdatePasswordRequest UpdatePasswordRequest
}

type UpdatePasswordRequest struct {
	Password       string `json:"password"`
	TokenPlaintext string `json:"token"`
}

// updatePasswordHandler swagger:route PUT /v1/users/password UsersV1 updatePassword
//
// # Updates the user's password
//
// User needs to send through the password reset token emailed to them.
//
//	Consumes:
//	- application/json
//
//	Produces:
//	- application/json
//
//	Schemes: http, https
//
//	Responses:
//	  202: messageResponse
//	  400: validationErrorResponse
//	  409: validationErrorResponse
func (api *UserV1Api) updatePasswordHandler(w http.ResponseWriter, r *http.Request) {
	var input UpdatePasswordRequest
	err := util.ReadJSON(w, r, &input)
	if err != nil {
		api.restHelper.BadRequestResponse(w, r, err)
		return
	}

	err = api.userUseCase.UpdatePassword(r.Context(), input.Password, input.TokenPlaintext)
	if err != nil {
		var validationErr *validator.ValidationError
		switch {
		case errors.As(err, &validationErr):
			api.log.Error("UserV1Api - updatePasswordHandler - userUseCase.UpdatePassword", "err", err)
			api.restHelper.FailedValidationResponse(w, r, validationErr.Errors)
		case errors.Is(err, error2.ErrEditConflict):
			api.restHelper.EditConflictResponse(w, r)
		default:
			api.restHelper.ServerErrorResponse(w, r, err)
		}
		return
	}

	env := util.MessageResponse{Message: "your password was successfully reset"}
	err = util.WriteJSON(w, http.StatusOK, env, nil)
	if err != nil {
		api.restHelper.ServerErrorResponse(w, r, err)
	}
}
