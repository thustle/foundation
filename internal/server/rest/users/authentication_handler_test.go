package users

import (
	"bytes"
	"encoding/json"
	"errors"
	error2 "foundation/internal/domain/error"
	"foundation/internal/domain/user"
	"foundation/internal/server/rest/util"
	"foundation/internal/validator"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"log/slog"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"
)

func TestAuthenticateUserHandler(t *testing.T) {
	testCases := []struct {
		name           string
		token          *user.Token
		tokenErr       error
		expectedStatus int
	}{
		{
			name:           "Token returned -> OK",
			token:          &user.Token{Plaintext: "ABC123"},
			tokenErr:       nil,
			expectedStatus: http.StatusOK,
		},
		{
			name:           "Invalid Credentials -> Unauthorized",
			token:          nil,
			tokenErr:       error2.InvalidCredentials,
			expectedStatus: http.StatusUnauthorized,
		},
		{
			name:           "Validation Err -> Bad Request",
			token:          nil,
			tokenErr:       &validator.ValidationError{},
			expectedStatus: http.StatusBadRequest,
		},
		{
			name:           "Other Err -> Internal Server Error",
			token:          nil,
			tokenErr:       errors.New("new error"),
			expectedStatus: http.StatusInternalServerError,
		},
	}
	log := slog.Default()
	wg := sync.WaitGroup{}
	helper := util.NewHelper(log, &wg)
	requestObject := &AuthRequest{Email: "user@company.com", Password: "pw123"}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {

			rr := httptest.NewRecorder()
			routePath := "/v1/users/authentication"
			body, _ := json.Marshal(requestObject)
			req, err := http.NewRequest(http.MethodPost, routePath, bytes.NewReader(body))
			if err != nil {
				t.Fatal(err)
			}
			mockUsrService := mockUserService{}
			mockUsrService.On("Authenticate", mock.Anything, requestObject.Email, requestObject.Password).
				Return(tc.token, tc.tokenErr).
				Once()

			api := &UserV1Api{&mockUsrService, nil, log, helper, &wg}
			api.authenticationHandler(rr, req)

			assert.Equal(t, tc.expectedStatus, rr.Code)
		})
	}
}

func TestAuthenticateUserHandlerBadRequestData(t *testing.T) {
	log := slog.Default()
	wg := sync.WaitGroup{}
	helper := util.NewHelper(log, &wg)

	rr := httptest.NewRecorder()
	routePath := "/v1/users/authentication"
	body := []byte(`{"email:":invalid,123}`)
	req, err := http.NewRequest(http.MethodPost, routePath, bytes.NewReader(body))
	if err != nil {
		t.Fatal(err)
	}
	mockUsrService := mockUserService{}
	api := &UserV1Api{&mockUsrService, nil, log, helper, &wg}
	api.authenticationHandler(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
}
