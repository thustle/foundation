package users

import (
	"errors"
	error2 "foundation/internal/domain/error"
	"foundation/internal/server/rest/util"
	"foundation/internal/validator"
	"net/http"
)

// swagger:parameters passwordReset
type PasswordResetRequestWrapper struct {
	// in: body
	// required: true
	// example: {"email:"james@company.com"}
	PasswordResetRequest PasswordResetRequest
}

type PasswordResetRequest struct {
	Email string `json:"email"`
}

// createPasswordResetTokenHandler swagger:route POST /v1/users/password-reset UsersV1 passwordReset
//
// Sends a password reset token to the user.
//
// User will be emailed a password reset token.
//
//	Consumes:
//	- application/json
//
//	Produces:
//	- application/json
//
//	Schemes: http, https
//
//	Responses:
//	  202: messageResponse
//	  400: validationErrorResponse
//	  404: validationErrorResponse
func (api *UserV1Api) createPasswordResetTokenHandler(w http.ResponseWriter, r *http.Request) {
	var input PasswordResetRequest
	err := util.ReadJSON(w, r, &input)
	if err != nil {
		api.restHelper.BadRequestResponse(w, r, err)
		return
	}

	_, err = api.userUseCase.CreatePasswordResetToken(r.Context(), input.Email)
	if err != nil {
		var validationErr *validator.ValidationError
		switch {
		case errors.As(err, &validationErr):
			api.log.Error("UserV1Api - createPasswordResetToken - userUseCase.CreatePasswordResetToken", "err", err)
			api.restHelper.FailedValidationResponse(w, r, validationErr.Errors)
		case errors.Is(err, error2.ErrRecordNotFound):
			api.restHelper.NotFoundResponse(w, r)
		default:
			api.restHelper.ServerErrorResponse(w, r, err)
		}
		return
	}

	err = util.WriteJSON(w, http.StatusAccepted, util.MessageResponse{Message: "password reset accepted"}, nil)
	if err != nil {
		api.restHelper.ServerErrorResponse(w, r, err)
	}
}
