package users

import (
	"foundation/config"
	"foundation/internal/domain/user"
	"foundation/internal/server/rest/util"
	"log/slog"
	"net/http"
	"sync"
)

type UserV1Api struct {
	userUseCase         user.Service
	config              *config.Config
	log                 *slog.Logger
	restHelper          *util.Helper
	backgroundWaitGroup *sync.WaitGroup
}

var userV1Once sync.Once
var userV1Api *UserV1Api

func NewUserV1Api(userUseCase user.Service, cfg *config.Config, log *slog.Logger, helper *util.Helper, wg *sync.WaitGroup) *UserV1Api {
	userV1Once.Do(func() {
		userV1Api = &UserV1Api{
			userUseCase:         userUseCase,
			config:              cfg,
			log:                 log,
			restHelper:          helper,
			backgroundWaitGroup: wg,
		}
	})
	return userV1Api
}

func (api *UserV1Api) ConfigureRoutes(mux *http.ServeMux, handler func(http.Handler) http.Handler) {
	mux.Handle("POST /v1/users", handler(http.HandlerFunc(api.registerUserHandler)))
	mux.Handle("PUT /v1/users/activated", handler(http.HandlerFunc(api.activateUserHandler)))
	mux.Handle("POST /v1/users/password-reset", handler(http.HandlerFunc(api.createPasswordResetTokenHandler)))
	mux.Handle("PUT /v1/users/password", handler(http.HandlerFunc(api.updatePasswordHandler)))
	mux.Handle("POST /v1/users/activation", handler(http.HandlerFunc(api.createActivationTokenHandler)))
	mux.Handle("POST /v1/users/authentication", handler(http.HandlerFunc(api.authenticationHandler)))
}
