package healthcheck

import (
	"foundation/config"
	"foundation/internal/server/rest/util"
	"net/http"
	"sync"
)

type V1Api struct {
	config     *config.Config
	restHelper *util.Helper
}

var healthCheckOnce sync.Once
var healthCheckV1Api *V1Api

func NewHealthCheckV1API(cfg *config.Config, helper *util.Helper) *V1Api {
	healthCheckOnce.Do(func() {
		healthCheckV1Api = &V1Api{
			config:     cfg,
			restHelper: helper,
		}
	})
	return healthCheckV1Api
}

func (app *V1Api) ConfigureRoutes(mux *http.ServeMux, handler func(http.Handler) http.Handler) {
	mux.Handle("GET /v1/health", handler(http.HandlerFunc(app.healthcheckHandler)))
}

// swagger:response healthResponse
type HealthResponseWrapper struct {
	HealthResponse HealthResponse
}

type HealthResponse struct {
	Status      string `json:"status"`
	Environment string `json:"environment"`
	Version     string `json:"version"`
}

// healthcheckHandler swagger:route GET /v1/health HealthcheckV1 healthcheck
//
// Activates the user on the system.
//
// User provides their activation token which they received via email to activate their account.
//
//	Consumes:
//	- application/json
//
//	Produces:
//	- application/json
//
//	Schemes: http, https
//
//	Responses:
//	  200: healthResponse
func (app *V1Api) healthcheckHandler(w http.ResponseWriter, r *http.Request) {
	data := HealthResponse{
		Status:      "available",
		Environment: app.config.Environment,
		Version:     app.config.Version,
	}
	err := util.WriteJSON(w, http.StatusOK, data, nil)
	if err != nil {
		app.restHelper.ServerErrorResponse(w, r, err)
	}
}
