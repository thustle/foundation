package util

import (
	"encoding/json"
	"errors"
	"fmt"
	"foundation/internal/validator"
	"io"
	"log/slog"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"sync"
)

type Envelope map[string]any

// swagger:response validationErrorResponse
type ValidationErrorResponseWrapper struct {
	ValidationErrorResponse ValidationErrorResponse
}

type ValidationErrorResponse struct {
	Errors map[string]string `json:"errors"`
}

// swagger:response messageResponse
type MessageResponseWrapper struct {
	MessageResponse MessageResponse
}

type MessageResponse struct {
	Message string `json:"message"`
}

type Helper struct {
	log *slog.Logger
	wg  *sync.WaitGroup
}

func NewHelper(log *slog.Logger, group *sync.WaitGroup) *Helper {
	return &Helper{
		log: log,
		wg:  group,
	}
}

func ReadString(qs url.Values, key, defaultValue string) string {
	s := qs.Get(key)
	if s == "" {
		return defaultValue
	}
	return s
}

func ReadCsv(qs url.Values, key string, defaultValue []string) []string {
	csv := qs.Get(key)
	if csv == "" {
		return defaultValue
	}
	return strings.Split(csv, ",")
}

func ReadInt(qs url.Values, key string, defaultValue int, v *validator.Validator) int {
	s := qs.Get(key)
	if s == "" {
		return defaultValue
	}
	i, err := strconv.Atoi(s)
	if err != nil {
		v.AddError(key, "must be an integer value")
		return defaultValue
	}
	return i
}

func WriteJSON(w http.ResponseWriter, status int, data any, headers http.Header) error {
	js, err := json.Marshal(data)
	if err != nil {
		return err
	}
	js = append(js, '\n')

	for key, value := range headers {
		w.Header()[key] = value
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	_, err = w.Write(js)
	if err != nil {
		return err
	}
	return nil
}

func (h *Helper) logError(r *http.Request, err error) {
	h.log.Error(err.Error(), "request_method", r.Method, "request_url", r.URL.String())
}

func (h *Helper) errorResponseString(w http.ResponseWriter, r *http.Request, status int, strMessage string) {
	message := map[string]string{"-": strMessage}
	h.errorResponse(w, r, status, message)
}

func (h *Helper) errorResponse(w http.ResponseWriter, r *http.Request, status int, message map[string]string) {
	env := ValidationErrorResponse{Errors: message}
	err := WriteJSON(w, status, env, nil)
	if err != nil {
		h.logError(r, err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func (h *Helper) ServerErrorResponse(w http.ResponseWriter, r *http.Request, err error) {
	h.logError(r, err)
	message := "the server encountered a problem and could not process your request"
	h.errorResponseString(w, r, http.StatusInternalServerError, message)
}

func (h *Helper) NotFoundResponse(w http.ResponseWriter, r *http.Request) {
	message := "the requested resource could not be found"
	h.errorResponseString(w, r, http.StatusNotFound, message)
}

func (h *Helper) BadRequestResponse(w http.ResponseWriter, r *http.Request, err error) {
	h.errorResponseString(w, r, http.StatusBadRequest, err.Error())
}

func (h *Helper) FailedValidationResponse(w http.ResponseWriter, r *http.Request, errors map[string]string) {
	h.errorResponse(w, r, http.StatusBadRequest, errors)
}

func (h *Helper) EditConflictResponse(w http.ResponseWriter, r *http.Request) {
	message := "unable to update the record due to an edit conflict, please try again"
	h.errorResponseString(w, r, http.StatusConflict, message)
}

func (h *Helper) RateLimitExceededResponse(w http.ResponseWriter, r *http.Request) {
	message := "rate limit exceeded"
	h.errorResponseString(w, r, http.StatusTooManyRequests, message)
}

func (h *Helper) InvalidCredentialsResponse(w http.ResponseWriter, r *http.Request) {
	message := "invalid authentication credentials"
	h.errorResponseString(w, r, http.StatusUnauthorized, message)
}

func (h *Helper) InvalidAuthenticationTokenResponse(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("WWW-Authenticate", "Bearer")
	message := "invalid or missing authentication token"
	h.errorResponseString(w, r, http.StatusUnauthorized, message)
}

func (h *Helper) AuthenticationRequiredResponse(w http.ResponseWriter, r *http.Request) {
	message := "you must be authenticated to access this resource"
	h.errorResponseString(w, r, http.StatusUnauthorized, message)
}
func (h *Helper) InactiveAccountResponse(w http.ResponseWriter, r *http.Request) {
	message := "your user account must be activated to access this resource"
	h.errorResponseString(w, r, http.StatusForbidden, message)
}

func (h *Helper) NotPermittedResponse(w http.ResponseWriter, r *http.Request) {
	message := "your user account doesn't have the necessary permissions to access this resource"
	h.errorResponseString(w, r, http.StatusForbidden, message)
}

func Background(log *slog.Logger, wg *sync.WaitGroup, fn func()) {
	wg.Add(1)
	go func() {
		defer wg.Done()
		defer func() {
			if err := recover(); err != nil {
				log.Error("Error in background process", "err", err)
			}
		}()
		fn()
	}()
}

func ReadJSON(w http.ResponseWriter, r *http.Request, dst any) error {
	maxBytes := 1_048_576
	r.Body = http.MaxBytesReader(w, r.Body, int64(maxBytes))
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()

	err := decoder.Decode(dst)
	if err != nil {
		var syntaxError *json.SyntaxError
		var unmarshalTypeError *json.UnmarshalTypeError
		var invalidUnmarshalError *json.InvalidUnmarshalError
		var maxBytesError *http.MaxBytesError
		switch {
		case errors.As(err, &syntaxError):
			return fmt.Errorf("body contains badly formed JSON (at character %d)", syntaxError.Offset)
		case errors.Is(err, io.ErrUnexpectedEOF):
			return errors.New("body contains badly formed JSON")
		case errors.As(err, &unmarshalTypeError):
			if unmarshalTypeError.Field != "" {
				return fmt.Errorf("body contains incorrect JSON type for field %q", unmarshalTypeError.Field)
			}
			return fmt.Errorf("body contains incorrect JSON type (at character %d)", unmarshalTypeError.Offset)
		case errors.Is(err, io.EOF):
			return errors.New("body must not be empty")
		case strings.HasPrefix(err.Error(), "json: unknown field "):
			fieldName := strings.TrimPrefix(err.Error(), "json: unknown field ")
			return fmt.Errorf("body contains unknown key %q", fieldName)
		case errors.As(err, &maxBytesError):
			return fmt.Errorf("body must not be larger than %d bytes", maxBytesError.Limit)
		case errors.As(err, &invalidUnmarshalError):
			panic(err)
		default:
			return err
		}
	}

	err = decoder.Decode(&struct{}{})
	if !errors.Is(err, io.EOF) {
		return errors.New("body must only contain a single JSON body")
	}
	return nil
}
