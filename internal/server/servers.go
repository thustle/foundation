package server

import (
	"foundation/config"
	"foundation/internal/domain"
	"foundation/internal/server/rest"
	"foundation/internal/server/rest/util"
	"log/slog"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

func StartServers(config *config.Config, log *slog.Logger, services *domain.Services, waitGroup *sync.WaitGroup) error {
	shutdownError := make(chan error)
	helper := util.NewHelper(log, waitGroup)
	srv := rest.New(config, log, helper, services, waitGroup)

	quit := make(chan os.Signal, 1)
	go func() {
		signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
		s := <-quit
		log.Info("Caught signal, shutting down", "signal", s.String())

		err := srv.Shutdown()
		if err != nil {
			shutdownError <- err
		}
		log.Info("completing background tasks", "addr", config.Port)
		waitGroup.Wait()
		shutdownError <- nil
	}()

	log.Info("Starting server", "Name", config.Name, "addr", config.Port)
	srv.Start()
	err := <-srv.Notify()
	quit <- syscall.SIGINT
	err = <-shutdownError
	if err != nil {
		return err
	}
	log.Info("Stopped server", "addr", config.Port)
	return nil
}
