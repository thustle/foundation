package validator

import "fmt"

type ValidationError struct {
	Errors map[string]string
}

func (v ValidationError) Error() string {
	return fmt.Sprintf("Validation errors: %v", v.Errors)
}

func NewValidationError(errors map[string]string) *ValidationError {
	return &ValidationError{
		Errors: errors,
	}
}
