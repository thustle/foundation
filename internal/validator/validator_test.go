package validator

import (
	"testing"
)

func TestPermittedValue(t *testing.T) {
	tests := []struct {
		name          string
		value         interface{}
		permittedVals []interface{}
		want          bool
	}{
		{
			name:          "FoundInPermitted",
			value:         10,
			permittedVals: []interface{}{10, 20, 30},
			want:          true,
		},
		{
			name:          "NotFoundInPermitted",
			value:         50,
			permittedVals: []interface{}{10, 20, 30},
			want:          false,
		},

		{
			name:          "EmptyPermitted",
			value:         10,
			permittedVals: nil,
			want:          false,
		},
		{
			name:          "PermittedWithDifferentTypes",
			value:         10,
			permittedVals: []interface{}{"10", 20, 30},
			want:          false,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			got := PermittedValue(tc.value, tc.permittedVals...)
			if got != tc.want {
				t.Errorf("PermittedValue() = %v; want = %v", got, tc.want)
			}
		})
	}
}

func TestUnique(t *testing.T) {
	var tests = []struct {
		name   string
		input  []int
		expect bool
	}{
		{
			name:   "allUnique",
			input:  []int{1, 2, 3, 4, 5},
			expect: true,
		},
		{
			name:   "notAllUnique",
			input:  []int{1, 2, 2, 4, 5},
			expect: false,
		},
		{
			name:   "emptySlice",
			input:  []int{},
			expect: true,
		},
		{
			name:   "nilSlice",
			input:  nil,
			expect: true,
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			result := Unique(tt.input)
			if result != tt.expect {
				t.Fatalf("expected %v, but got %v", tt.expect, result)
			}
		})
	}
}
