package customer

import (
	"context"
	"errors"
	"foundation/internal/domain/base"
	"foundation/internal/domain/data"
	error2 "foundation/internal/domain/error"
	"foundation/internal/validator"
	"log/slog"
	"sync"
)

type Service interface {
	Create(context.Context, *Customer) error
	Search(ctx context.Context, req SearchRequest) ([]*Customer, data.Metadata, error)
	GetByID(ctx context.Context, id int64) (*Customer, error)
}

type UseCase struct {
	base.Service
	repository Repository
}

func NewService(repository Repository, log *slog.Logger, wg *sync.WaitGroup) *UseCase {
	return &UseCase{
		repository: repository,
		Service: base.Service{
			Log:                 log,
			BackgroundWaitGroup: wg,
		},
	}
}

func (s *UseCase) Create(ctx context.Context, customer *Customer) error {
	v := validator.New()
	if validateCustomer(v, customer); !v.Valid() {
		return validator.NewValidationError(v.Errors)
	}

	err := s.repository.Insert(ctx, customer)
	if err != nil {
		switch {
		case errors.Is(err, error2.ErrDuplicateEmail):
			v.AddError("email", "email address already in use")
			return validator.NewValidationError(v.Errors)
		default:
			return err
		}
	}
	return nil
}

type SearchRequest struct {
	Forename string
	Surname  string
	Email    string
	Phone    string
	data.Filters
}

func (s *UseCase) Search(ctx context.Context, req SearchRequest) ([]*Customer, data.Metadata, error) {
	v := validator.New()
	if data.ValidateFilters(v, req.Filters); !v.Valid() {
		return nil, data.Metadata{}, validator.NewValidationError(v.Errors)
	}
	return s.repository.Search(ctx, req)
}

func (s *UseCase) GetByID(ctx context.Context, id int64) (*Customer, error) {
	return s.repository.GetByID(ctx, id)
}

func validateCustomer(v *validator.Validator, c *Customer) {
	v.Check(c.Forename != "", "forename", "must be provided")
	v.Check(len(c.Forename) <= 500, "forename", "must not be greater than 500 bytes long")
	v.Check(c.Surname != "", "surname", "must be provided")
	v.Check(len(c.Forename) <= 500, "surname", "must not be greater than 500 bytes long")
	if c.Email != nil {
		validator.ValidateEmail(v, *c.Email)
	}
	if c.Phone != nil {
		v.Check(len(*c.Phone) <= 500, "phone", "must not be greater than 500 bytes long")
	}
}
