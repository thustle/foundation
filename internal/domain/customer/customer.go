package customer

import (
	"encoding/json"
	"strings"
	"time"
)

type JsonBirthDate time.Time

type Customer struct {
	ID        int64          `json:"id"`
	CreatedAt time.Time      `json:"created_at"`
	CreatedBy *int64         `json:"-"`
	Forename  string         `json:"forename"`
	Surname   string         `json:"surname"`
	DOB       *JsonBirthDate `json:"dob"`
	Email     *string        `json:"email"`
	Phone     *string        `json:"phone"`
	Version   int            `json:"-"`
}

func (j *JsonBirthDate) UnmarshalJSON(b []byte) error {
	s := strings.Trim(string(b), "\"")
	t, err := time.Parse("2006-01-02", s)
	if err != nil {
		return err
	}
	*j = JsonBirthDate(t)
	return nil
}

func (j *JsonBirthDate) MarshalJSON() ([]byte, error) {
	return json.Marshal(time.Time(*j))
}

func (j *JsonBirthDate) Format(s string) string {
	t := time.Time(*j)
	return t.Format(s)
}

func (j *JsonBirthDate) asTime() *time.Time {
	if j == nil {
		return nil
	} else {
		return (*time.Time)(j)
	}
}
