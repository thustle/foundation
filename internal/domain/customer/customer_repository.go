package customer

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"foundation/internal/db"
	context2 "foundation/internal/domain/context"
	"foundation/internal/domain/data"
	error2 "foundation/internal/domain/error"
	"time"
)

type Repository interface {
	Insert(context.Context, *Customer) error
	Search(context.Context, SearchRequest) ([]*Customer, data.Metadata, error)
	GetByID(context.Context, int64) (*Customer, error)
}

type PostgresCustomerRepository struct {
	*db.Postgres
}

func New(pg *db.Postgres) *PostgresCustomerRepository {
	return &PostgresCustomerRepository{pg}
}

func (r *PostgresCustomerRepository) Insert(ctx context.Context, customer *Customer) error {
	if usr, ok := context2.GetUser(ctx); ok {
		customer.CreatedBy = &usr.ID
	}
	dobDate := customer.DOB.asTime()
	query := `INSERT INTO customers (created_by, forename, surname, dob, email, phone) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id, created_at, version`
	args := []any{customer.CreatedBy, customer.Forename, customer.Surname, dobDate, customer.Email, customer.Phone}
	ctx, cancel := context.WithTimeout(ctx, 3*time.Second)
	defer cancel()

	row := r.DB.QueryRowContext(ctx, query, args...)
	err := row.Scan(&customer.ID, &customer.CreatedAt, &customer.Version)
	if err != nil {
		switch {
		case err.Error() == `pq: duplicate key value violates unique constraint "customers_email_key"`:
			return error2.ErrDuplicateEmail
		default:
			return err
		}
	}
	return nil
}

func (r *PostgresCustomerRepository) Search(ctx context.Context, req SearchRequest) ([]*Customer, data.Metadata, error) {
	filters := req.Filters
	query := fmt.Sprintf(`SELECT count(*) OVER(), id, created_at, forename, surname, dob, email, phone, version
		FROM customers
		WHERE (to_tsvector('simple', forename) @@ plainto_tsquery('simple', $1) OR $1 = '')
		AND (to_tsvector('simple', surname) @@ plainto_tsquery('simple', $2) OR $2 = '')
		AND (to_tsvector('simple', email) @@ plainto_tsquery('simple', $3) OR $3 = '')
		AND (to_tsvector('simple', phone) @@ plainto_tsquery('simple', $4) OR $4 = '')
		ORDER BY %s %s, id ASC
		LIMIT $5 OFFSET $6`, filters.SortColumn(), filters.SortDirection())
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	rows, err := r.DB.QueryContext(ctx, query, req.Forename, req.Surname, req.Email, req.Phone, filters.Limit(), filters.Offset())
	if err != nil {
		return nil, data.Metadata{}, err
	}
	defer rows.Close()

	var customers []*Customer
	totalRecords := 0
	for rows.Next() {
		var movie Customer
		err := rows.Scan(
			&totalRecords,
			&movie.ID,
			&movie.CreatedAt,
			&movie.Forename,
			&movie.Surname,
			&movie.DOB,
			&movie.Email,
			&movie.Phone,
			&movie.Version,
		)
		if err != nil {
			return nil, data.Metadata{}, err
		}
		customers = append(customers, &movie)
	}
	if err = rows.Err(); err != nil {
		return nil, data.Metadata{}, err
	}
	metadata := data.CalculateMetadata(totalRecords, filters.Page, filters.PageSize)
	return customers, metadata, nil
}

func (r *PostgresCustomerRepository) GetByID(ctx context.Context, id int64) (*Customer, error) {
	query := `SELECT id, created_at, forename, surname, dob, email, phone, version FROM customers WHERE id = $1`
	var customer Customer
	ctx, cancel := context.WithTimeout(ctx, 3*time.Second)
	defer cancel()

	err := r.DB.QueryRowContext(ctx, query, id).Scan(
		&customer.ID,
		&customer.CreatedAt,
		&customer.Forename,
		&customer.Surname,
		&customer.DOB,
		&customer.Email,
		&customer.Phone,
		&customer.Version,
	)
	if err != nil {
		switch {
		case errors.Is(err, sql.ErrNoRows):
			return nil, error2.ErrRecordNotFound
		default:
			return nil, err
		}
	}
	return &customer, nil
}
