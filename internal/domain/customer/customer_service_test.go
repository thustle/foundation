package customer

import (
	"context"
	"errors"
	db2 "foundation/internal/db"
	"foundation/internal/domain/data"
	error2 "foundation/internal/domain/error"
	"foundation/internal/validator"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"log/slog"
	"sync"
	"testing"
	"time"
)

func TestCreate(t *testing.T) {
	now := time.Now()
	phone := "+391234568"
	email := "cust@email.com"
	testCases := []struct {
		name, forename, surname string
		email, phone            *string
		dob                     *time.Time
		sqlExpectations         func(sqlmock2 sqlmock.Sqlmock)
		expectedError           error
	}{
		{
			name:     "Success",
			forename: "Forename",
			surname:  "Customer",
			dob:      &now,
			email:    &email,
			phone:    &phone,
			sqlExpectations: func(mock sqlmock.Sqlmock) {
				customerMockRows := sqlmock.NewRows([]string{"id", "created_at", "version"}).
					AddRow("5", time.Now(), 1)
				mock.ExpectQuery("INSERT INTO customers (.+)").WillReturnRows(customerMockRows)
			},
			expectedError: nil,
		},
		{
			name:     "Duplicate Email",
			forename: "Forename",
			surname:  "Customer",
			dob:      &now,
			email:    &email,
			phone:    &phone,
			sqlExpectations: func(mock sqlmock.Sqlmock) {
				mock.ExpectQuery("INSERT INTO customers (.+)").WillReturnError(errors.New(`pq: duplicate key value violates unique constraint "customers_email_key"`))
			},
			expectedError: validator.NewValidationError(map[string]string{"email": "email address already in use"}),
		},
		{
			name:            "Missing Data",
			forename:        "",
			surname:         "",
			dob:             nil,
			email:           nil,
			phone:           nil,
			sqlExpectations: func(mock sqlmock.Sqlmock) {},
			expectedError: validator.NewValidationError(map[string]string{
				"forename": "must be provided",
				"surname":  "must be provided",
			}),
		},
		{
			name:            "Bad Email",
			forename:        "Forename",
			surname:         "Customer",
			dob:             &now,
			email:           &phone,
			phone:           &phone,
			sqlExpectations: func(mock sqlmock.Sqlmock) {},
			expectedError:   validator.NewValidationError(map[string]string{"email": "must be a valid email address"}),
		},
	}

	for _, tc := range testCases {
		customer := &Customer{
			Forename: tc.forename,
			Surname:  tc.surname,
			DOB:      (*JsonBirthDate)(tc.dob),
			Email:    tc.email,
			Phone:    tc.phone,
		}

		t.Run(tc.name, func(t *testing.T) {
			db, mock, err := sqlmock.New()
			if err != nil {
				t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
			}
			defer db.Close()

			pgDb := &db2.Postgres{DB: db}
			customerRepo := New(pgDb)
			wg := sync.WaitGroup{}
			tc.sqlExpectations(mock)

			service := NewService(customerRepo, slog.Default(), &wg)
			err = service.Create(context.Background(), customer)

			assert.Equal(t, tc.expectedError, err)
		})
	}
}

func TestSearch(t *testing.T) {
	now := time.Now()
	phone := "+391234568"
	email := "cust@email.com"
	testCases := []struct {
		name            string
		input           SearchRequest
		sqlExpectations func(sqlmock2 sqlmock.Sqlmock)
		expectedError   error
	}{
		{
			name: "Success",
			input: SearchRequest{
				Forename: "Forename",
				Surname:  "Customer",
				Email:    email,
				Phone:    phone,
				Filters: data.Filters{
					Sort:         "-email",
					Page:         1,
					PageSize:     20,
					SortSafelist: []string{"email", "-email"},
				},
			},
			sqlExpectations: func(mock sqlmock.Sqlmock) {
				customerMockRows := sqlmock.NewRows([]string{"count", "id", "created_at", "forename", "surname", "dob", "email", "phone", "version"}).
					AddRow(1, 1, now, "fore", "sur", now, "email", "phone", 4)
				mock.ExpectQuery("SELECT (.+) FROM customers").WillReturnRows(customerMockRows)
			},
			expectedError: nil,
		},
		{
			name: "Invalid sort",
			input: SearchRequest{
				Forename: "Forename",
				Surname:  "Customer",
				Email:    email,
				Phone:    phone,
				Filters: data.Filters{
					Sort:     "-emails",
					Page:     1,
					PageSize: 20,
				},
			},
			sqlExpectations: func(mock sqlmock.Sqlmock) {},
			expectedError:   validator.NewValidationError(map[string]string{"sort": "invalid sort value"}),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			db, mock, err := sqlmock.New()
			if err != nil {
				t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
			}
			defer db.Close()

			pgDb := &db2.Postgres{DB: db}
			customerRepo := New(pgDb)
			wg := sync.WaitGroup{}
			tc.sqlExpectations(mock)

			service := NewService(customerRepo, slog.Default(), &wg)
			_, md, err := service.Search(context.Background(), tc.input)

			if err != nil {
				assert.Equal(t, tc.expectedError, err)
			} else {
				assert.Equal(t, tc.input.Filters.Page, md.CurrentPage)
				assert.Equal(t, tc.input.Filters.PageSize, md.PageSize)
			}
		})
	}
}

func TestGetById(t *testing.T) {
	now := time.Now()
	testCases := []struct {
		name            string
		sqlExpectations func(sqlmock2 sqlmock.Sqlmock)
		expectedError   error
	}{
		{
			name: "Success",
			sqlExpectations: func(mock sqlmock.Sqlmock) {
				customerMockRows := sqlmock.NewRows([]string{"id", "created_at", "forename", "surname", "dob", "email", "phone", "version"}).
					AddRow(int64(1), now, "fore", "sur", now, "email", "phone", 4)
				mock.ExpectQuery("SELECT (.+) FROM customers").WillReturnRows(customerMockRows)
			},
			expectedError: nil,
		},
		{
			name: "Not Found",
			sqlExpectations: func(mock sqlmock.Sqlmock) {
				noRows := sqlmock.NewRows([]string{"id", "created_at", "forename", "surname", "dob", "email", "phone", "version"})
				mock.ExpectQuery("SELECT (.+) FROM customers").WillReturnRows(noRows)
			},
			expectedError: error2.ErrRecordNotFound,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			db, mock, err := sqlmock.New()
			if err != nil {
				t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
			}
			defer db.Close()

			pgDb := &db2.Postgres{DB: db}
			customerRepo := New(pgDb)
			wg := sync.WaitGroup{}
			tc.sqlExpectations(mock)

			service := NewService(customerRepo, slog.Default(), &wg)
			customer, err := service.GetByID(context.Background(), 3)

			if err != nil {
				assert.Equal(t, tc.expectedError, err)
			} else {
				assert.Equal(t, int64(1), customer.ID)
			}
		})
	}
}
