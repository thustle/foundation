package email

import (
	"bytes"
	"embed"
	"foundation/config"
	"github.com/go-mail/mail/v2"
	"html/template"
	"log/slog"
	"time"
)

//go:embed "templates"
var templateFS embed.FS

type Service interface {
	Send(recipient, templateFile string, data any) error
}

type Mailer struct {
	enabled bool
	dialer  *mail.Dialer
	sender  string
	log     *slog.Logger
}

func NewService(cfg *config.Config, log *slog.Logger) *Mailer {
	dialer := mail.NewDialer(cfg.SMTPHost, cfg.SMTPPort, cfg.SMTPUsername, cfg.SMTPPassword)
	dialer.Timeout = 5 * time.Second
	return &Mailer{
		enabled: cfg.SMTPEnabled,
		dialer:  dialer,
		sender:  cfg.SMTPSender,
		log:     log,
	}
}

func (m Mailer) Send(recipient, templateFile string, data any) error {
	if !m.enabled {
		return nil
	}
	// Use the ParseFS() method to parse the required template file from the embedded file system.
	tmpl, err := template.New("email").ParseFS(templateFS, "templates/"+templateFile)
	if err != nil {
		return err
	}
	// Execute the named template "subject", passing in the dynamic data and storing the result in a bytes.Buffer variable.
	subject := new(bytes.Buffer)
	err = tmpl.ExecuteTemplate(subject, "subject", data)
	if err != nil {
		return err
	}

	plainBody := new(bytes.Buffer)
	err = tmpl.ExecuteTemplate(plainBody, "plainBody", data)
	if err != nil {
		return err
	}
	htmlBody := new(bytes.Buffer)
	err = tmpl.ExecuteTemplate(htmlBody, "htmlBody", data)
	if err != nil {
		return err
	}

	msg := mail.NewMessage()
	msg.SetHeader("To", recipient)
	msg.SetHeader("From", m.sender)
	msg.SetHeader("Subject", subject.String())
	msg.SetBody("text/plain", plainBody.String())
	msg.AddAlternative("text/html", htmlBody.String())

	// Try sending the email up to three times before aborting and returning the final
	// error. We sleep for 700 milliseconds between each attempt.
	for i := 1; i <= 3; i++ {
		err = m.dialer.DialAndSend(msg)
		if nil == err {
			m.log.Info("Email sent", "recipient", recipient, "subject", subject.String())
			return nil
		}
		// If it didn't work, sleep for a short time and retry.
		time.Sleep(700 * time.Millisecond)
	}
	return err
}
