package context

import (
	"context"
	"foundation/internal/domain/user"
)

type contextKey string

const userContextKey = contextKey("user")

func SetUser(ctx context.Context, user *user.User) context.Context {
	return context.WithValue(ctx, userContextKey, user)
}

func GetUser(ctx context.Context) (*user.User, bool) {
	usr, ok := ctx.Value(userContextKey).(*user.User)
	return usr, ok
}
