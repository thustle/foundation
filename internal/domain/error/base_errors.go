package error

import "errors"

var (
	ErrDuplicateEmail  = errors.New("duplicate email")
	ErrRecordNotFound  = errors.New("record not found")
	ErrEditConflict    = errors.New("edit conflict")
	InvalidCredentials = errors.New("invalid credentials")
)
