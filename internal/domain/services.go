package domain

import (
	"foundation/config"
	"foundation/internal/db"
	"foundation/internal/domain/customer"
	"foundation/internal/domain/email"
	"foundation/internal/domain/user"
	"log/slog"
	"sync"
)

type Services struct {
	UserService     user.Service
	EmailService    email.Service
	CustomerService customer.Service
}

func BuildServices(dbConnection *db.Postgres, cfg *config.Config, log *slog.Logger, wg *sync.WaitGroup) *Services {
	emailService := email.NewService(cfg, log)
	userService := user.NewService(
		dbConnection.DB,
		user.New(dbConnection),
		user.NewPermissionsRepository(dbConnection),
		user.NewTokenRepository(dbConnection),
		emailService,
		log,
		wg,
	)
	customerService := customer.NewService(customer.New(dbConnection), log, wg)
	return &Services{
		UserService:     userService,
		EmailService:    emailService,
		CustomerService: customerService,
	}
}
