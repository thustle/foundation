package user

import (
	"context"
	"database/sql"
	"errors"
	"foundation/internal/domain/base"
	"foundation/internal/domain/email"
	error2 "foundation/internal/domain/error"
	"foundation/internal/server/rest/util"
	"foundation/internal/validator"
	"log/slog"
	"sync"
	"time"
)

type Service interface {
	GetByEmail(context.Context, string) (*User, error)
	Create(context.Context, *User) error
	ActivateUser(context.Context, string) (*User, error)
	CreatePasswordResetToken(context.Context, string) (*Token, error)
	UpdatePassword(context.Context, string, string) error
	CreateActivationToken(context.Context, string) error
	Authenticate(context.Context, string, string) (*Token, error)
	GetUserPermissions(context.Context, int64) (Permissions, error)
	GetByToken(context.Context, string, string) (*User, error)
}

type UseCase struct {
	base.Service
	db                    *sql.DB
	repository            Repository
	permissionsRepository PermissionsRepository
	tokenRepository       TokenRepository
	emailService          email.Service
}

func NewService(db *sql.DB, repository Repository, permissionsRepository PermissionsRepository, tokenRepository TokenRepository, emailService email.Service, log *slog.Logger, wg *sync.WaitGroup) *UseCase {
	return &UseCase{
		db:                    db,
		repository:            repository,
		permissionsRepository: permissionsRepository,
		tokenRepository:       tokenRepository,
		emailService:          emailService,
		Service: base.Service{
			Log:                 log,
			BackgroundWaitGroup: wg,
		},
	}
}

func (s *UseCase) GetByEmail(ctx context.Context, email string) (*User, error) {
	return s.repository.GetByEmail(ctx, email)
}

func (s *UseCase) GetUserPermissions(ctx context.Context, userId int64) (Permissions, error) {
	return s.permissionsRepository.GetAllForUser(ctx, userId)
}

func (s *UseCase) GetByToken(ctx context.Context, scope string, token string) (*User, error) {
	return s.repository.GetForToken(ctx, scope, token)
}

func (s *UseCase) Create(ctx context.Context, user *User) error {
	v := validator.New()
	if validateUser(v, user); !v.Valid() {
		return validator.NewValidationError(v.Errors)
	}

	tx, err := s.db.Begin()
	if err != nil {
		return err
	}
	defer func() { s.rollback(tx) }()

	err = s.repository.Insert(ctx, tx, user)
	if err != nil {
		switch {
		case errors.Is(err, error2.ErrDuplicateEmail):
			v.AddError("email", "email address already in use")
			return validator.NewValidationError(v.Errors)
		default:
			return err
		}
	}

	err = s.permissionsRepository.AddForUser(ctx, tx, user.ID, "movies:read")
	if err != nil {
		return err
	}

	token, err := s.tokenRepository.New(ctx, tx, user.ID, 3*24*time.Hour, ScopeActivation)
	if err != nil {
		return err
	}

	util.Background(s.Log, s.BackgroundWaitGroup, func() {
		templateData := map[string]any{
			"activationToken": token.Plaintext,
			"userID":          user.ID,
		}
		s.Log.Info("Emailing: ", "Data", templateData)
		err = s.emailService.Send(user.Email, "user_welcome.gohtml", templateData)
		if err != nil {
			s.Log.Error(err.Error(), "email", user.Email)
		}
	})

	err = tx.Commit()
	if err != nil {
		s.Log.Error("Error committing transaction", "err", err)
	}
	return nil
}

func (s *UseCase) ActivateUser(ctx context.Context, token string) (*User, error) {
	v := validator.New()
	if ValidateTokenPlaintext(v, token); !v.Valid() {
		return nil, validator.NewValidationError(v.Errors)
	}

	user, err := s.repository.GetForToken(ctx, ScopeActivation, token)
	if err != nil {
		switch {
		case errors.Is(err, error2.ErrRecordNotFound):
			v.AddError("token", "invalid or expired activation token")
			return nil, validator.NewValidationError(v.Errors)
		default:
			return nil, err
		}
	}

	user.Activated = true
	err = s.repository.Update(ctx, user)
	if err != nil {
		return nil, err
	}

	err = s.tokenRepository.DeleteAllForUser(ctx, ScopeActivation, user.ID)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (s *UseCase) CreatePasswordResetToken(ctx context.Context, email string) (*Token, error) {
	v := validator.New()
	if validator.ValidateEmail(v, email); !v.Valid() {
		return nil, validator.NewValidationError(v.Errors)
	}

	user, err := s.repository.GetByEmail(ctx, email)
	if err != nil {
		return nil, err
	}
	if !user.Activated {
		v.AddError("email", "user account must be activated")
		return nil, validator.NewValidationError(v.Errors)
	}

	token, err := s.tokenRepository.New(ctx, nil, user.ID, 45*time.Minute, ScopePasswordReset)
	if err != nil {
		return nil, err
	}

	util.Background(s.Log, s.BackgroundWaitGroup, func() {
		templateData := map[string]any{
			"passwordResetToken": token.Plaintext,
		}
		err = s.emailService.Send(user.Email, "token_password_reset.gohtml", templateData)
		if err != nil {
			s.Log.Error("Error sending password reset token", "email", email, "err", err)
		}
	})
	return token, nil
}

func (s *UseCase) UpdatePassword(ctx context.Context, password, token string) error {
	v := validator.New()
	validatePasswordPlaintext(v, password)
	ValidateTokenPlaintext(v, token)
	if !v.Valid() {
		return validator.NewValidationError(v.Errors)
	}

	user, err := s.repository.GetForToken(ctx, ScopePasswordReset, token)
	if err != nil {
		switch {
		case errors.Is(err, error2.ErrRecordNotFound):
			v.AddError("token", "invalid or expired password reset token")
			return validator.NewValidationError(v.Errors)
		default:
			return err
		}
	}

	err = user.Password.Set(password)
	if err != nil {
		return err
	}
	err = s.repository.Update(ctx, user)
	if err != nil {
		return err
	}

	err = s.tokenRepository.DeleteAllForUser(ctx, ScopePasswordReset, user.ID)
	if err != nil {
		return err
	}
	return nil
}

func (s *UseCase) CreateActivationToken(ctx context.Context, email string) error {
	v := validator.New()
	if validator.ValidateEmail(v, email); !v.Valid() {
		return validator.NewValidationError(v.Errors)
	}

	user, err := s.repository.GetByEmail(ctx, email)
	if err != nil {
		switch {
		case errors.Is(err, error2.ErrRecordNotFound):
			v.AddError("email", "no matching email address found")
			return validator.NewValidationError(v.Errors)
		default:
			return err
		}
	}
	if user.Activated {
		v.AddError("email", "user has already been activated")
		return validator.NewValidationError(v.Errors)
	}

	token, err := s.tokenRepository.New(ctx, nil, user.ID, 3*24*time.Hour, ScopeActivation)
	if err != nil {
		return err
	}

	util.Background(s.Log, s.BackgroundWaitGroup, func() {
		templateData := map[string]any{
			"activationToken": token.Plaintext,
		}
		err = s.emailService.Send(user.Email, "token_activation.gohtml", templateData)
		if err != nil {
			s.Log.Error("Error sending token_activation email to user", "email", email, "err", err)
		}
	})
	return nil
}

func (s *UseCase) Authenticate(ctx context.Context, email, password string) (*Token, error) {
	v := validator.New()
	validator.ValidateEmail(v, email)
	validatePasswordPlaintext(v, password)
	if !v.Valid() {
		return nil, validator.NewValidationError(v.Errors)
	}

	user, err := s.repository.GetByEmail(ctx, email)
	if err != nil {
		switch {
		case errors.Is(err, error2.ErrRecordNotFound):
			return nil, error2.InvalidCredentials
		default:
			return nil, err
		}
	}

	matches, err := user.Password.Matches(password)
	if err != nil {
		return nil, err
	}
	if !matches {
		return nil, error2.InvalidCredentials
	}

	token, err := s.tokenRepository.New(ctx, nil, user.ID, 24*time.Hour, ScopeAuthentication)
	if err != nil {
		return nil, err
	}
	return token, nil
}

func (s *UseCase) rollback(tx *sql.Tx) {
	err := tx.Rollback()
	if err == nil {
		s.Log.Info("Rolled back transaction")
	} else if !errors.Is(err, sql.ErrTxDone) {
		s.Log.Error("Error rolling back transaction", "err", err)
	} else {
		s.Log.Info("Transaction was completed")
	}
}
