package user

import (
	"context"
	"database/sql"
	"fmt"
	"foundation/internal/db"
	"github.com/lib/pq"
	"time"
)

type PermissionsRepository interface {
	GetAllForUser(ctx context.Context, userID int64) (Permissions, error)
	AddForUser(ctx context.Context, tx *sql.Tx, userID int64, codes ...string) error
}

type PostgresPermissionsRepository struct {
	*db.Postgres
}

func NewPermissionsRepository(pg *db.Postgres) *PostgresPermissionsRepository {
	return &PostgresPermissionsRepository{pg}
}

func (p *PostgresPermissionsRepository) GetAllForUser(ctx context.Context, userID int64) (Permissions, error) {
	query := `SELECT permissions.code
			FROM permissions
			INNER JOIN users_permissions ON users_permissions.permission_id = permissions.id
			INNER JOIN users ON users_permissions.user_id = users.id
			WHERE users.id = $1`
	ctx, cancel := context.WithTimeout(ctx, 3*time.Second)
	defer cancel()

	rows, err := p.DB.QueryContext(ctx, query, userID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var permissions Permissions
	for rows.Next() {
		var permission string
		err := rows.Scan(&permission)
		if err != nil {
			return nil, err
		}
		permissions = append(permissions, permission)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return permissions, nil
}

func (p *PostgresPermissionsRepository) AddForUser(ctx context.Context, tx *sql.Tx, userID int64, codes ...string) error {
	query := `INSERT INTO users_permissions
			SELECT $1, permissions.id FROM permissions WHERE permissions.code = ANY($2)`
	ctx, cancel := context.WithTimeout(ctx, 3*time.Second)
	defer cancel()

	var err error
	if tx != nil {
		_, err = tx.ExecContext(ctx, query, userID, pq.Array(codes))
	} else {
		_, err = p.DB.ExecContext(ctx, query, userID, pq.Array(codes))
	}
	if err != nil {
		return fmt.Errorf("permissionsRepository - AddForUser - r.Builder: %w", err)
	}
	return nil
}
