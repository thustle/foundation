package user

import (
	"context"
	"database/sql/driver"
	"errors"
	db2 "foundation/internal/db"
	error2 "foundation/internal/domain/error"
	"foundation/internal/validator"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"log/slog"
	"sync"
	"testing"
	"time"
)

type mockEmailService struct {
	recipient, templateFile string
	data                    any
}

func (m *mockEmailService) Send(recipient, templateFile string, data any) error {
	m.recipient = recipient
	m.templateFile = templateFile
	m.data = data
	return nil
}

func TestCreate(t *testing.T) {
	testCases := []struct {
		name, username, email, password string
		sqlExpectations                 func(sqlmock2 sqlmock.Sqlmock)
		expectedError                   error
	}{
		{
			name:     "Success",
			username: "test",
			email:    "test@company.com",
			password: "password1",
			sqlExpectations: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				articleMockRows := sqlmock.NewRows([]string{"id", "created_at", "version"}).
					AddRow("5", time.Now(), 1)
				mock.ExpectQuery("INSERT INTO users (.+)").WillReturnRows(articleMockRows)
				mock.ExpectExec("INSERT INTO users_permissions").WillReturnResult(driver.RowsAffected(1))
				mock.ExpectExec("INSERT INTO tokens (.+)").WillReturnResult(driver.RowsAffected(1))
				mock.ExpectCommit()
			},
			expectedError: nil,
		},
		{
			name:     "Duplicate Email",
			username: "test",
			email:    "test@company.com",
			password: "password1",
			sqlExpectations: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.ExpectQuery("INSERT INTO users (.+)").WillReturnError(errors.New(`pq: duplicate key value violates unique constraint "users_email_key"`))
				mock.ExpectRollback()
			},
			expectedError: validator.NewValidationError(map[string]string{"email": "email address already in use"}),
		},
		{
			name:            "Missing Data",
			username:        "",
			email:           "",
			password:        "",
			sqlExpectations: func(mock sqlmock.Sqlmock) {},
			expectedError: validator.NewValidationError(map[string]string{
				"name":     "must be provided",
				"email":    "must be provided",
				"password": "must be provided",
			}),
		},
		{
			name:            "Password Length Error",
			username:        "test",
			email:           "test@company.com",
			password:        "pw",
			sqlExpectations: func(mock sqlmock.Sqlmock) {},
			expectedError:   validator.NewValidationError(map[string]string{"password": "must be at least 8 bytes long"}),
		},
		{
			name:            "Bad Email",
			username:        "test",
			email:           "badco",
			password:        "password1",
			sqlExpectations: func(mock sqlmock.Sqlmock) {},
			expectedError:   validator.NewValidationError(map[string]string{"email": "must be a valid email address"}),
		},
	}

	for _, tc := range testCases {
		user := &User{
			Name:  tc.username,
			Email: tc.email,
		}
		err := user.Password.Set(tc.password)
		if err != nil {
			t.Error(err)
		}

		t.Run(tc.name, func(t *testing.T) {
			db, mock, err := sqlmock.New()
			if err != nil {
				t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
			}
			defer db.Close()

			pgDb := &db2.Postgres{DB: db}
			userRepo := New(pgDb)
			tokenRepo := NewTokenRepository(pgDb)
			PermissionsRepo := NewPermissionsRepository(pgDb)
			emailService := &mockEmailService{}
			wg := sync.WaitGroup{}
			tc.sqlExpectations(mock)

			service := NewService(db, userRepo, PermissionsRepo, tokenRepo, emailService, slog.Default(), &wg)
			err = service.Create(context.Background(), user)

			assert.Equal(t, tc.expectedError, err)
		})
	}
}

func TestActivateUser(t *testing.T) {
	testCases := []struct {
		name, token       string
		sqlExpectations   func(sqlmock2 sqlmock.Sqlmock)
		expectedError     error
		expectedActivated bool
	}{
		{
			name:  "Success",
			token: "12345678901234567890123456",
			sqlExpectations: func(mock sqlmock.Sqlmock) {
				userMockRows := sqlmock.NewRows([]string{"id", "created_at", "name", "email", "password_hash", "activated", "version"}).
					AddRow("5", time.Now(), "a", "a@a.c", "asd", false, 1)
				mock.ExpectQuery("SELECT (.+) FROM users INNER JOIN tokens").WillReturnRows(userMockRows)
				articleUpdateRows := sqlmock.NewRows([]string{"version"}).AddRow(2)
				mock.ExpectQuery("UPDATE users (.+)").WillReturnRows(articleUpdateRows)
				mock.ExpectExec("DELETE FROM tokens").WillReturnResult(driver.RowsAffected(1))
			},
			expectedError:     nil,
			expectedActivated: true,
		},
		{
			name:  "Not Found",
			token: "12345678901234567890123456",
			sqlExpectations: func(mock sqlmock.Sqlmock) {
				noRows := sqlmock.NewRows([]string{"id", "created_at", "name", "email", "password_hash", "activated", "version"})
				mock.ExpectQuery("SELECT (.+) FROM users INNER JOIN tokens").WillReturnRows(noRows)
			},
			expectedError:     validator.NewValidationError(map[string]string{"token": "invalid or expired activation token"}),
			expectedActivated: false,
		},
		{
			name:              "Bad Token",
			token:             "",
			sqlExpectations:   func(mock sqlmock.Sqlmock) {},
			expectedError:     validator.NewValidationError(map[string]string{"token": "must be provided"}),
			expectedActivated: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			db, mock, err := sqlmock.New()
			if err != nil {
				t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
			}
			defer db.Close()

			pgDb := &db2.Postgres{DB: db}
			userRepo := New(pgDb)
			tokenRepo := NewTokenRepository(pgDb)
			PermissionsRepo := NewPermissionsRepository(pgDb)
			emailService := &mockEmailService{}
			wg := sync.WaitGroup{}
			tc.sqlExpectations(mock)

			service := NewService(db, userRepo, PermissionsRepo, tokenRepo, emailService, slog.Default(), &wg)
			usr, err := service.ActivateUser(context.Background(), tc.token)

			assert.Equal(t, tc.expectedError, err)
			if tc.expectedActivated {
				assert.True(t, usr.Activated)
			}
		})
	}
}

func TestGetByEmail(t *testing.T) {
	testCases := []struct {
		name, email     string
		sqlExpectations func(sqlmock2 sqlmock.Sqlmock)
		expectedError   error
	}{
		{
			name:  "Success",
			email: "test@test.com",
			sqlExpectations: func(mock sqlmock.Sqlmock) {
				userMockRows := sqlmock.NewRows([]string{"id", "created_at", "name", "email", "password_hash", "activated", "version"}).
					AddRow("5", time.Now(), "a", "a@a.c", "asd", false, 1)
				mock.ExpectQuery("SELECT (.+) FROM users WHERE email").WillReturnRows(userMockRows)
			},
			expectedError: nil,
		},
		{
			name:  "Not Found",
			email: "test@test.com",
			sqlExpectations: func(mock sqlmock.Sqlmock) {
				noRows := sqlmock.NewRows([]string{"id", "created_at", "name", "email", "password_hash", "activated", "version"})
				mock.ExpectQuery("SELECT (.+) FROM users WHERE email").WillReturnRows(noRows)
			},
			expectedError: error2.ErrRecordNotFound,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			db, mock, err := sqlmock.New()
			if err != nil {
				t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
			}
			defer db.Close()

			pgDb := &db2.Postgres{DB: db}
			userRepo := New(pgDb)
			tokenRepo := NewTokenRepository(pgDb)
			PermissionsRepo := NewPermissionsRepository(pgDb)
			emailService := &mockEmailService{}
			wg := sync.WaitGroup{}
			tc.sqlExpectations(mock)

			service := NewService(db, userRepo, PermissionsRepo, tokenRepo, emailService, slog.Default(), &wg)
			_, err = service.GetByEmail(context.Background(), tc.email)

			assert.Equal(t, tc.expectedError, err)
		})
	}
}
