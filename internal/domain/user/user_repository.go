package user

import (
	"context"
	"crypto/sha256"
	"database/sql"
	"errors"
	"foundation/internal/db"
	error2 "foundation/internal/domain/error"
	"time"
)

type Repository interface {
	Insert(context.Context, *sql.Tx, *User) error
	GetByEmail(context.Context, string) (*User, error)
	Update(context.Context, *User) error
	GetForToken(context.Context, string, string) (*User, error)
}

type PostgresUserRepository struct {
	*db.Postgres
}

func New(pg *db.Postgres) *PostgresUserRepository {
	return &PostgresUserRepository{pg}
}

func (r *PostgresUserRepository) Insert(ctx context.Context, tx *sql.Tx, user *User) error {
	query := `INSERT INTO users (name, email, password_hash, activated) VALUES ($1, $2, $3, $4) RETURNING id, created_at, version`
	args := []any{user.Name, user.Email, user.Password.Hash, user.Activated}
	ctx, cancel := context.WithTimeout(ctx, 3*time.Second)
	defer cancel()

	var row *sql.Row
	if tx != nil {
		row = tx.QueryRowContext(ctx, query, args...)
	} else {
		row = r.DB.QueryRowContext(ctx, query, args...)
	}
	err := row.Scan(&user.ID, &user.CreatedAt, &user.Version)
	if err != nil {
		switch {
		case err.Error() == `pq: duplicate key value violates unique constraint "users_email_key"`:
			return error2.ErrDuplicateEmail
		default:
			return err
		}
	}
	return nil
}

func (r *PostgresUserRepository) Update(ctx context.Context, user *User) error {
	query := `UPDATE users SET name = $1, email = $2, password_hash = $3, activated = $4, version = version + 1
			WHERE id = $5 and version = $6 RETURNING version`
	args := []any{
		user.Name,
		user.Email,
		user.Password.Hash,
		user.Activated,
		user.ID,
		user.Version,
	}
	ctx, cancel := context.WithTimeout(ctx, 3*time.Second)
	defer cancel()

	err := r.DB.QueryRowContext(ctx, query, args...).Scan(&user.Version)
	if err != nil {
		switch {
		case err.Error() == `pq: duplicate key value violates unique constraint "users_email_key"`:
			return error2.ErrDuplicateEmail
		default:
			return err
		}
	}
	return nil
}

func (r *PostgresUserRepository) GetByEmail(ctx context.Context, email string) (*User, error) {
	query := `SELECT id, created_at, name, email, password_hash, activated, version FROM users WHERE email = $1`
	var user User
	ctx, cancel := context.WithTimeout(ctx, 3*time.Second)
	defer cancel()

	err := r.DB.QueryRowContext(ctx, query, email).Scan(
		&user.ID,
		&user.CreatedAt,
		&user.Name,
		&user.Email,
		&user.Password.Hash,
		&user.Activated,
		&user.Version,
	)
	if err != nil {
		switch {
		case errors.Is(err, sql.ErrNoRows):
			return nil, error2.ErrRecordNotFound
		default:
			return nil, err
		}
	}
	return &user, nil
}

func (r *PostgresUserRepository) GetForToken(ctx context.Context, tokenScope, tokenPlaintext string) (*User, error) {
	tokenHash := sha256.Sum256([]byte(tokenPlaintext))
	query := `SELECT users.id, users.created_at, users.name, users.email, users.password_hash, users.activated, users.version
			FROM users
			INNER JOIN tokens ON users.id = tokens.user_id
			WHERE tokens.hash = $1 AND tokens.scope = $2 AND tokens.expiry > $3`
	args := []any{tokenHash[:], tokenScope, time.Now()}
	var user User
	ctx, cancel := context.WithTimeout(ctx, 3*time.Second)
	defer cancel()

	err := r.DB.QueryRowContext(ctx, query, args...).Scan(
		&user.ID,
		&user.CreatedAt,
		&user.Name,
		&user.Email,
		&user.Password.Hash,
		&user.Activated,
		&user.Version,
	)
	if err != nil {
		switch {
		case errors.Is(err, sql.ErrNoRows):
			return nil, error2.ErrRecordNotFound
		default:
			return nil, err
		}
	}
	// Return the matching user.
	return &user, nil
}
