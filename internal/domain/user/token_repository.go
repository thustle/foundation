package user

import (
	"context"
	"crypto/rand"
	"crypto/sha256"
	"database/sql"
	"encoding/base32"
	"fmt"
	"foundation/internal/db"
	"time"
)

type TokenRepository interface {
	New(context.Context, *sql.Tx, int64, time.Duration, string) (*Token, error)
	Insert(context.Context, *sql.Tx, *Token) error
	DeleteAllForUser(context.Context, string, int64) error
}

type PostgresTokenRepository struct {
	*db.Postgres
}

func NewTokenRepository(pg *db.Postgres) *PostgresTokenRepository {
	return &PostgresTokenRepository{pg}
}

func (r *PostgresTokenRepository) New(ctx context.Context, tx *sql.Tx, userId int64, ttl time.Duration, scope string) (*Token, error) {
	token, err := generateToken(userId, ttl, scope)
	if err != nil {
		return nil, err
	}
	err = r.Insert(ctx, tx, token)
	return token, err
}

func (r *PostgresTokenRepository) Insert(ctx context.Context, tx *sql.Tx, token *Token) error {
	query := `INSERT INTO tokens (hash, user_id, expiry, scope) VALUES ($1, $2, $3, $4)`
	args := []any{token.Hash, token.UserID, token.Expiry, token.Scope}
	ctx, cancel := context.WithTimeout(ctx, 3*time.Second)
	defer cancel()

	var err error
	if tx != nil {
		_, err = tx.ExecContext(ctx, query, args...)
	} else {
		_, err = r.DB.ExecContext(ctx, query, args...)
	}
	if err != nil {
		return fmt.Errorf("tokenRepository - Insert - r.Pool.Exec: %w", err)
	}
	return nil
}

func (r *PostgresTokenRepository) DeleteAllForUser(ctx context.Context, scope string, userID int64) error {
	query := `DELETE FROM tokens WHERE scope = $1 AND user_id = $2`
	ctx, cancel := context.WithTimeout(ctx, 3*time.Second)
	defer cancel()

	_, err := r.DB.ExecContext(ctx, query, scope, userID)
	return err
}

func generateToken(userID int64, ttl time.Duration, scope string) (*Token, error) {
	token := &Token{
		UserID: userID,
		Scope:  scope,
		Expiry: time.Now().Add(ttl),
	}
	randomBytes := make([]byte, 16)
	_, err := rand.Read(randomBytes)
	if err != nil {
		return nil, err
	}
	token.Plaintext = base32.StdEncoding.WithPadding(base32.NoPadding).EncodeToString(randomBytes)
	hash := sha256.Sum256([]byte(token.Plaintext))
	token.Hash = hash[:]
	return token, nil
}
