package user

import "foundation/internal/validator"

func validateUser(v *validator.Validator, user *User) {
	v.Check(user.Name != "", "name", "must be provided")
	v.Check(len(user.Name) <= 500, "name", "must not be greater than 500 bytes long")
	validator.ValidateEmail(v, user.Email)
	if user.Password.plaintext != nil {
		validatePasswordPlaintext(v, *user.Password.plaintext)
	}
	if user.Password.Hash == nil {
		panic("missing password hash for user")
	}
}

func validatePasswordPlaintext(v *validator.Validator, password string) {
	v.Check(password != "", "password", "must be provided")
	v.Check(len(password) >= 8, "password", "must be at least 8 bytes long")
	v.Check(len(password) <= 72, "password", "must not be more than 72 bytes long")
}

func ValidateTokenPlaintext(v *validator.Validator, tokenPlaintext string) {
	v.Check(tokenPlaintext != "", "token", "must be provided")
	v.Check(len(tokenPlaintext) == 26, "token", "must be 26 bytes long")
}
