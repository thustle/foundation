package base

import (
	"log/slog"
	"sync"
)

type Service struct {
	Log                 *slog.Logger
	BackgroundWaitGroup *sync.WaitGroup
}
