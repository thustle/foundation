package config

import (
	"fmt"
	"os"
	"path/filepath"
	"runtime"

	"github.com/ilyakaznacheev/cleanenv"
)

type (
	// Config -.
	Config struct {
		App     `yaml:"app"`
		HTTP    `yaml:"http"`
		Log     `yaml:"logger"`
		PG      `yaml:"postgres"`
		Limiter `yaml:"limiter"`
		SMTP    `yaml:"smtp"`
		CORS    `yaml:"cors"`
	}

	// App -.
	App struct {
		Name        string `env-required:"true" yaml:"name"    env:"APP_NAME"`
		Version     string `env-required:"true" yaml:"version" env:"APP_VERSION"`
		Environment string `env-required:"true" yaml:"env" env:"APP_ENVIRONMENT"`
	}

	// HTTP -.
	HTTP struct {
		Port            string `env-required:"true" yaml:"port" env:"HTTP_PORT"`
		ReadTimeout     int    `env-required:"true" yaml:"read_timeout_secs" env:"HTTP_READ_TIMEOUT"`
		WriteTimeout    int    `env-required:"true" yaml:"write_timeout_secs" env:"HTTP_WRITE_TIMEOUT"`
		ShutdownTimeout int    `env-required:"true" yaml:"shutdown_timeout_secs" env:"HTTP_SHUTDOWN_TIMEOUT"`
	}

	// Log -.
	Log struct {
		Level string `env-required:"true" yaml:"log_level" env:"LOG_LEVEL"`
	}

	// PG -.
	PG struct {
		PoolMax     int    `env-required:"true" yaml:"pool_max" env:"PG_POOL_MAX"`
		URL         string `env-required:"true" yaml:"url" env:"PG_URL"`
		MaxIdle     int    `env-required:"true" yaml:"max_idle" env:"PG_MAX_IDLE"`
		MaxIdleTime string `env-required:"true" yaml:"max_idle_time" env:"PG_MAX_IDLE_TIME"`
	}

	// Limiter -.
	Limiter struct {
		Rps     float64 `env-required:"true" yaml:"rps" env:"LIMITER_RPS"`
		Burst   int     `env-required:"true" yaml:"burst" env:"LIMITER_BURST"`
		Enabled bool    `env-required:"true" yaml:"enabled" env:"LIMITER_ENABLED"`
	}

	SMTP struct {
		SMTPEnabled  bool   `env-default:"true" yaml:"enabled" env:"SMTP_ENABLED"`
		SMTPHost     string `env-required:"true" yaml:"host" env:"SMTP_HOST"`
		SMTPPort     int    `env-required:"true" yaml:"port" env:"SMTP_PORT"`
		SMTPUsername string `env-required:"true" yaml:"username" env:"SMTP_USERNAME"`
		SMTPPassword string `env-required:"true" yaml:"password" env:"SMTP_PASSWORD"`
		SMTPSender   string `env-required:"true" yaml:"sender" env:"SMTP_SENDER"`
	}

	CORS struct {
		TrustedOrigins string `env-default:"" yaml:"trusted_origins" env:"CORS_TRUSTED_ORIGINS"`
	}
)

func NewConfig() *Config {
	cfg := &Config{}
	cwd := projectRoot()
	envFilePath := cwd + "config.yml"

	err := readEnv(envFilePath, cfg)
	if err != nil {
		panic(err)
	}

	return cfg
}

func readEnv(envFilePath string, cfg *Config) error {
	envFileExists := checkFileExists(envFilePath)

	if envFileExists {
		err := cleanenv.ReadConfig(envFilePath, cfg)
		if err != nil {
			return fmt.Errorf("config error: %w", err)
		}
	} else {
		err := cleanenv.ReadEnv(cfg)
		if err != nil {

			if _, statErr := os.Stat(envFilePath + ".example"); statErr == nil {
				return fmt.Errorf("missing environmentvariables: %w\n\nprovide all required environment variables or rename and update .env.example to .env for convinience", err)
			}

			return err
		}
	}
	return nil
}

func checkFileExists(fileName string) bool {
	envFileExists := false
	if _, err := os.Stat(fileName); err == nil {
		envFileExists = true
	}
	return envFileExists
}

func projectRoot() string {
	_, b, _, _ := runtime.Caller(0)
	projectRoot := filepath.Dir(b)

	return projectRoot + "/../"
}
