package main

import (
	"fmt"
	"foundation/config"
	"foundation/internal/db"
	"foundation/internal/domain"
	"foundation/internal/logger"
	"foundation/internal/server"
	"io"
	"os"
	"sync"
)

func main() {
	if err := run(os.Stdout); err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(1)
	}
}

func run(w io.Writer) error {
	configuration := config.NewConfig()
	log := logger.New(configuration, w)
	dbConnection, err := createDbConnection(configuration)
	if err != nil {
		log.Error("Error establishing DB connection", err)
		return err
	}
	waitGroup := sync.WaitGroup{}
	services := domain.BuildServices(dbConnection, configuration, log, &waitGroup)
	return server.StartServers(configuration, log, services, &waitGroup)
}

func createDbConnection(config *config.Config) (*db.Postgres, error) {
	database := db.NewOrGetSingleton(config)
	err := db.RunMigrations(config.PG.URL)
	if err != nil {
		return nil, err
	}
	return database, nil
}
