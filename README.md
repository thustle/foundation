# Foundation

A 'foundation' for an API server written in Go. This source is partially based 
on the excellent "Let's Go Further" book, but expands on it.

#### Swagger interface:
![screenshot of swagger interface](screenshot-swagger.png)

## Additions
- [x] Customer add/search endpoints.
- [x] Separation of HTTP server logic (in the server package) and business logic (in domain).
- [x] Ability to specify numerous 'servers' (currently there is only an HTTP one, but others can be added and all started up together when the app starts up and can share the existing business layer logic).
- [x] Unit tests.
- [x] Integration tests, using godog to run cucumber features to test the API. Testcontainers is used to spin up a test Postgres and Mailhog instance.
- [x] Open API specification generation and swagger UI.

## Installation
- You will need to have [Go](https://go.dev/doc/install) 1.22 or later installed in order to build the application
- The system expects the following services to be accessible: Postgres (for the database), and an SMTP server (e.g. the integration tests use Mailhog)
- By default, migrations will run automatically on the supplied database.

## License
[MIT](LICENSE)